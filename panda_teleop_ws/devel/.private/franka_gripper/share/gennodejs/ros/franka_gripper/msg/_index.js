
"use strict";

let HomingResult = require('./HomingResult.js');
let HomingActionFeedback = require('./HomingActionFeedback.js');
let StopResult = require('./StopResult.js');
let StopActionFeedback = require('./StopActionFeedback.js');
let StopFeedback = require('./StopFeedback.js');
let StopGoal = require('./StopGoal.js');
let HomingFeedback = require('./HomingFeedback.js');
let StopActionGoal = require('./StopActionGoal.js');
let HomingActionGoal = require('./HomingActionGoal.js');
let GraspActionResult = require('./GraspActionResult.js');
let HomingAction = require('./HomingAction.js');
let GraspGoal = require('./GraspGoal.js');
let GraspFeedback = require('./GraspFeedback.js');
let MoveActionResult = require('./MoveActionResult.js');
let MoveActionGoal = require('./MoveActionGoal.js');
let GraspResult = require('./GraspResult.js');
let GraspActionFeedback = require('./GraspActionFeedback.js');
let GraspAction = require('./GraspAction.js');
let HomingGoal = require('./HomingGoal.js');
let GraspActionGoal = require('./GraspActionGoal.js');
let MoveFeedback = require('./MoveFeedback.js');
let MoveActionFeedback = require('./MoveActionFeedback.js');
let MoveResult = require('./MoveResult.js');
let StopActionResult = require('./StopActionResult.js');
let HomingActionResult = require('./HomingActionResult.js');
let StopAction = require('./StopAction.js');
let MoveAction = require('./MoveAction.js');
let MoveGoal = require('./MoveGoal.js');
let GraspEpsilon = require('./GraspEpsilon.js');

module.exports = {
  HomingResult: HomingResult,
  HomingActionFeedback: HomingActionFeedback,
  StopResult: StopResult,
  StopActionFeedback: StopActionFeedback,
  StopFeedback: StopFeedback,
  StopGoal: StopGoal,
  HomingFeedback: HomingFeedback,
  StopActionGoal: StopActionGoal,
  HomingActionGoal: HomingActionGoal,
  GraspActionResult: GraspActionResult,
  HomingAction: HomingAction,
  GraspGoal: GraspGoal,
  GraspFeedback: GraspFeedback,
  MoveActionResult: MoveActionResult,
  MoveActionGoal: MoveActionGoal,
  GraspResult: GraspResult,
  GraspActionFeedback: GraspActionFeedback,
  GraspAction: GraspAction,
  HomingGoal: HomingGoal,
  GraspActionGoal: GraspActionGoal,
  MoveFeedback: MoveFeedback,
  MoveActionFeedback: MoveActionFeedback,
  MoveResult: MoveResult,
  StopActionResult: StopActionResult,
  HomingActionResult: HomingActionResult,
  StopAction: StopAction,
  MoveAction: MoveAction,
  MoveGoal: MoveGoal,
  GraspEpsilon: GraspEpsilon,
};
