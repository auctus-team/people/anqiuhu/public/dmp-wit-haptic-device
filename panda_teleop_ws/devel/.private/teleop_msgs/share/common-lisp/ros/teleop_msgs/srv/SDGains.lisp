; Auto-generated. Do not edit!


(cl:in-package teleop_msgs-srv)


;//! \htmlinclude SDGains-request.msg.html

(cl:defclass <SDGains-request> (roslisp-msg-protocol:ros-message)
  ((K_ac
    :reader K_ac
    :initarg :K_ac
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (B_ac
    :reader B_ac
    :initarg :B_ac
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass SDGains-request (<SDGains-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SDGains-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SDGains-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<SDGains-request> is deprecated: use teleop_msgs-srv:SDGains-request instead.")))

(cl:ensure-generic-function 'K_ac-val :lambda-list '(m))
(cl:defmethod K_ac-val ((m <SDGains-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:K_ac-val is deprecated.  Use teleop_msgs-srv:K_ac instead.")
  (K_ac m))

(cl:ensure-generic-function 'B_ac-val :lambda-list '(m))
(cl:defmethod B_ac-val ((m <SDGains-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:B_ac-val is deprecated.  Use teleop_msgs-srv:B_ac instead.")
  (B_ac m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SDGains-request>) ostream)
  "Serializes a message object of type '<SDGains-request>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'K_ac))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'K_ac))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'B_ac))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'B_ac))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SDGains-request>) istream)
  "Deserializes a message object of type '<SDGains-request>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'K_ac) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'K_ac)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'B_ac) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'B_ac)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SDGains-request>)))
  "Returns string type for a service object of type '<SDGains-request>"
  "teleop_msgs/SDGainsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SDGains-request)))
  "Returns string type for a service object of type 'SDGains-request"
  "teleop_msgs/SDGainsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SDGains-request>)))
  "Returns md5sum for a message object of type '<SDGains-request>"
  "da7436754c6eed502ac4bf2d5d8f6b56")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SDGains-request)))
  "Returns md5sum for a message object of type 'SDGains-request"
  "da7436754c6eed502ac4bf2d5d8f6b56")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SDGains-request>)))
  "Returns full string definition for message of type '<SDGains-request>"
  (cl:format cl:nil "float64[] K_ac~%float64[] B_ac~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SDGains-request)))
  "Returns full string definition for message of type 'SDGains-request"
  (cl:format cl:nil "float64[] K_ac~%float64[] B_ac~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SDGains-request>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'K_ac) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'B_ac) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SDGains-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SDGains-request
    (cl:cons ':K_ac (K_ac msg))
    (cl:cons ':B_ac (B_ac msg))
))
;//! \htmlinclude SDGains-response.msg.html

(cl:defclass <SDGains-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass SDGains-response (<SDGains-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SDGains-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SDGains-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<SDGains-response> is deprecated: use teleop_msgs-srv:SDGains-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <SDGains-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:success-val is deprecated.  Use teleop_msgs-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SDGains-response>) ostream)
  "Serializes a message object of type '<SDGains-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SDGains-response>) istream)
  "Deserializes a message object of type '<SDGains-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SDGains-response>)))
  "Returns string type for a service object of type '<SDGains-response>"
  "teleop_msgs/SDGainsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SDGains-response)))
  "Returns string type for a service object of type 'SDGains-response"
  "teleop_msgs/SDGainsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SDGains-response>)))
  "Returns md5sum for a message object of type '<SDGains-response>"
  "da7436754c6eed502ac4bf2d5d8f6b56")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SDGains-response)))
  "Returns md5sum for a message object of type 'SDGains-response"
  "da7436754c6eed502ac4bf2d5d8f6b56")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SDGains-response>)))
  "Returns full string definition for message of type '<SDGains-response>"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SDGains-response)))
  "Returns full string definition for message of type 'SDGains-response"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SDGains-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SDGains-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SDGains-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SDGains)))
  'SDGains-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SDGains)))
  'SDGains-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SDGains)))
  "Returns string type for a service object of type '<SDGains>"
  "teleop_msgs/SDGains")