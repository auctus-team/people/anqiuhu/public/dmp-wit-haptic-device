; Auto-generated. Do not edit!


(cl:in-package teleop_msgs-srv)


;//! \htmlinclude InterestPose-request.msg.html

(cl:defclass <InterestPose-request> (roslisp-msg-protocol:ros-message)
  ((interest_pose
    :reader interest_pose
    :initarg :interest_pose
    :type geometry_msgs-msg:PoseArray
    :initform (cl:make-instance 'geometry_msgs-msg:PoseArray)))
)

(cl:defclass InterestPose-request (<InterestPose-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <InterestPose-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'InterestPose-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<InterestPose-request> is deprecated: use teleop_msgs-srv:InterestPose-request instead.")))

(cl:ensure-generic-function 'interest_pose-val :lambda-list '(m))
(cl:defmethod interest_pose-val ((m <InterestPose-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:interest_pose-val is deprecated.  Use teleop_msgs-srv:interest_pose instead.")
  (interest_pose m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <InterestPose-request>) ostream)
  "Serializes a message object of type '<InterestPose-request>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'interest_pose) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <InterestPose-request>) istream)
  "Deserializes a message object of type '<InterestPose-request>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'interest_pose) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<InterestPose-request>)))
  "Returns string type for a service object of type '<InterestPose-request>"
  "teleop_msgs/InterestPoseRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'InterestPose-request)))
  "Returns string type for a service object of type 'InterestPose-request"
  "teleop_msgs/InterestPoseRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<InterestPose-request>)))
  "Returns md5sum for a message object of type '<InterestPose-request>"
  "353b04f6dc21618f6bc1a71e1c6743a9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'InterestPose-request)))
  "Returns md5sum for a message object of type 'InterestPose-request"
  "353b04f6dc21618f6bc1a71e1c6743a9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<InterestPose-request>)))
  "Returns full string definition for message of type '<InterestPose-request>"
  (cl:format cl:nil "geometry_msgs/PoseArray interest_pose~%~%================================================================================~%MSG: geometry_msgs/PoseArray~%# An array of poses with a header for global reference.~%~%Header header~%~%Pose[] poses~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'InterestPose-request)))
  "Returns full string definition for message of type 'InterestPose-request"
  (cl:format cl:nil "geometry_msgs/PoseArray interest_pose~%~%================================================================================~%MSG: geometry_msgs/PoseArray~%# An array of poses with a header for global reference.~%~%Header header~%~%Pose[] poses~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <InterestPose-request>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'interest_pose))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <InterestPose-request>))
  "Converts a ROS message object to a list"
  (cl:list 'InterestPose-request
    (cl:cons ':interest_pose (interest_pose msg))
))
;//! \htmlinclude InterestPose-response.msg.html

(cl:defclass <InterestPose-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass InterestPose-response (<InterestPose-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <InterestPose-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'InterestPose-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<InterestPose-response> is deprecated: use teleop_msgs-srv:InterestPose-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <InterestPose-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:success-val is deprecated.  Use teleop_msgs-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <InterestPose-response>) ostream)
  "Serializes a message object of type '<InterestPose-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <InterestPose-response>) istream)
  "Deserializes a message object of type '<InterestPose-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<InterestPose-response>)))
  "Returns string type for a service object of type '<InterestPose-response>"
  "teleop_msgs/InterestPoseResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'InterestPose-response)))
  "Returns string type for a service object of type 'InterestPose-response"
  "teleop_msgs/InterestPoseResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<InterestPose-response>)))
  "Returns md5sum for a message object of type '<InterestPose-response>"
  "353b04f6dc21618f6bc1a71e1c6743a9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'InterestPose-response)))
  "Returns md5sum for a message object of type 'InterestPose-response"
  "353b04f6dc21618f6bc1a71e1c6743a9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<InterestPose-response>)))
  "Returns full string definition for message of type '<InterestPose-response>"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'InterestPose-response)))
  "Returns full string definition for message of type 'InterestPose-response"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <InterestPose-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <InterestPose-response>))
  "Converts a ROS message object to a list"
  (cl:list 'InterestPose-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'InterestPose)))
  'InterestPose-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'InterestPose)))
  'InterestPose-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'InterestPose)))
  "Returns string type for a service object of type '<InterestPose>"
  "teleop_msgs/InterestPose")