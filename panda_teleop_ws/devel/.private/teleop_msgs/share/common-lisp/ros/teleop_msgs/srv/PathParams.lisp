; Auto-generated. Do not edit!


(cl:in-package teleop_msgs-srv)


;//! \htmlinclude PathParams-request.msg.html

(cl:defclass <PathParams-request> (roslisp-msg-protocol:ros-message)
  ((path_step
    :reader path_step
    :initarg :path_step
    :type cl:integer
    :initform 0))
)

(cl:defclass PathParams-request (<PathParams-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathParams-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathParams-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<PathParams-request> is deprecated: use teleop_msgs-srv:PathParams-request instead.")))

(cl:ensure-generic-function 'path_step-val :lambda-list '(m))
(cl:defmethod path_step-val ((m <PathParams-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:path_step-val is deprecated.  Use teleop_msgs-srv:path_step instead.")
  (path_step m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathParams-request>) ostream)
  "Serializes a message object of type '<PathParams-request>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_step)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'path_step)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'path_step)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'path_step)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathParams-request>) istream)
  "Deserializes a message object of type '<PathParams-request>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_step)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'path_step)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'path_step)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'path_step)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathParams-request>)))
  "Returns string type for a service object of type '<PathParams-request>"
  "teleop_msgs/PathParamsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathParams-request)))
  "Returns string type for a service object of type 'PathParams-request"
  "teleop_msgs/PathParamsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathParams-request>)))
  "Returns md5sum for a message object of type '<PathParams-request>"
  "5cd08abbeba16b0ad3a10ce92f788c97")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathParams-request)))
  "Returns md5sum for a message object of type 'PathParams-request"
  "5cd08abbeba16b0ad3a10ce92f788c97")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathParams-request>)))
  "Returns full string definition for message of type '<PathParams-request>"
  (cl:format cl:nil "uint32 path_step~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathParams-request)))
  "Returns full string definition for message of type 'PathParams-request"
  (cl:format cl:nil "uint32 path_step~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathParams-request>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathParams-request>))
  "Converts a ROS message object to a list"
  (cl:list 'PathParams-request
    (cl:cons ':path_step (path_step msg))
))
;//! \htmlinclude PathParams-response.msg.html

(cl:defclass <PathParams-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass PathParams-response (<PathParams-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathParams-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathParams-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<PathParams-response> is deprecated: use teleop_msgs-srv:PathParams-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <PathParams-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:success-val is deprecated.  Use teleop_msgs-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathParams-response>) ostream)
  "Serializes a message object of type '<PathParams-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathParams-response>) istream)
  "Deserializes a message object of type '<PathParams-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathParams-response>)))
  "Returns string type for a service object of type '<PathParams-response>"
  "teleop_msgs/PathParamsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathParams-response)))
  "Returns string type for a service object of type 'PathParams-response"
  "teleop_msgs/PathParamsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathParams-response>)))
  "Returns md5sum for a message object of type '<PathParams-response>"
  "5cd08abbeba16b0ad3a10ce92f788c97")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathParams-response)))
  "Returns md5sum for a message object of type 'PathParams-response"
  "5cd08abbeba16b0ad3a10ce92f788c97")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathParams-response>)))
  "Returns full string definition for message of type '<PathParams-response>"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathParams-response)))
  "Returns full string definition for message of type 'PathParams-response"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathParams-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathParams-response>))
  "Converts a ROS message object to a list"
  (cl:list 'PathParams-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'PathParams)))
  'PathParams-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'PathParams)))
  'PathParams-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathParams)))
  "Returns string type for a service object of type '<PathParams>"
  "teleop_msgs/PathParams")