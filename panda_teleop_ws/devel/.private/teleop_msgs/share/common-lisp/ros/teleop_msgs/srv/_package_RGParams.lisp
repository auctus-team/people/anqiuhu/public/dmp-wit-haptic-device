(cl:in-package teleop_msgs-srv)
(cl:export '(A-VAL
          A
          B-VAL
          B
          C-VAL
          C
          D_AC-VAL
          D_AC
          K_AC-VAL
          K_AC
          B_AC-VAL
          B_AC
          D_AC_OBS-VAL
          D_AC_OBS
          K_AC_OBS-VAL
          K_AC_OBS
          B_AC_OBS-VAL
          B_AC_OBS
          AC_TYPE-VAL
          AC_TYPE
          SUCCESS-VAL
          SUCCESS
))