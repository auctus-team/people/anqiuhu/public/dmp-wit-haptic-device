(cl:in-package teleop_msgs-srv)
(cl:export '(D_AC-VAL
          D_AC
          K_AC-VAL
          K_AC
          B_AC-VAL
          B_AC
          K_AC_OBS-VAL
          K_AC_OBS
          B_AC_OBS-VAL
          B_AC_OBS
          SUCCESS-VAL
          SUCCESS
))