; Auto-generated. Do not edit!


(cl:in-package teleop_msgs-srv)


;//! \htmlinclude GTParams-request.msg.html

(cl:defclass <GTParams-request> (roslisp-msg-protocol:ros-message)
  ((d_ac
    :reader d_ac
    :initarg :d_ac
    :type cl:float
    :initform 0.0)
   (K_ac
    :reader K_ac
    :initarg :K_ac
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (B_ac
    :reader B_ac
    :initarg :B_ac
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass GTParams-request (<GTParams-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GTParams-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GTParams-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<GTParams-request> is deprecated: use teleop_msgs-srv:GTParams-request instead.")))

(cl:ensure-generic-function 'd_ac-val :lambda-list '(m))
(cl:defmethod d_ac-val ((m <GTParams-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:d_ac-val is deprecated.  Use teleop_msgs-srv:d_ac instead.")
  (d_ac m))

(cl:ensure-generic-function 'K_ac-val :lambda-list '(m))
(cl:defmethod K_ac-val ((m <GTParams-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:K_ac-val is deprecated.  Use teleop_msgs-srv:K_ac instead.")
  (K_ac m))

(cl:ensure-generic-function 'B_ac-val :lambda-list '(m))
(cl:defmethod B_ac-val ((m <GTParams-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:B_ac-val is deprecated.  Use teleop_msgs-srv:B_ac instead.")
  (B_ac m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GTParams-request>) ostream)
  "Serializes a message object of type '<GTParams-request>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'd_ac))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'K_ac))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'K_ac))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'B_ac))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'B_ac))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GTParams-request>) istream)
  "Deserializes a message object of type '<GTParams-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'd_ac) (roslisp-utils:decode-double-float-bits bits)))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'K_ac) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'K_ac)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'B_ac) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'B_ac)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GTParams-request>)))
  "Returns string type for a service object of type '<GTParams-request>"
  "teleop_msgs/GTParamsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GTParams-request)))
  "Returns string type for a service object of type 'GTParams-request"
  "teleop_msgs/GTParamsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GTParams-request>)))
  "Returns md5sum for a message object of type '<GTParams-request>"
  "2d7aed71a95980a2d156ffd0840bab20")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GTParams-request)))
  "Returns md5sum for a message object of type 'GTParams-request"
  "2d7aed71a95980a2d156ffd0840bab20")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GTParams-request>)))
  "Returns full string definition for message of type '<GTParams-request>"
  (cl:format cl:nil "float64 d_ac~%float64[] K_ac~%float64[] B_ac~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GTParams-request)))
  "Returns full string definition for message of type 'GTParams-request"
  (cl:format cl:nil "float64 d_ac~%float64[] K_ac~%float64[] B_ac~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GTParams-request>))
  (cl:+ 0
     8
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'K_ac) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'B_ac) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GTParams-request>))
  "Converts a ROS message object to a list"
  (cl:list 'GTParams-request
    (cl:cons ':d_ac (d_ac msg))
    (cl:cons ':K_ac (K_ac msg))
    (cl:cons ':B_ac (B_ac msg))
))
;//! \htmlinclude GTParams-response.msg.html

(cl:defclass <GTParams-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass GTParams-response (<GTParams-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GTParams-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GTParams-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name teleop_msgs-srv:<GTParams-response> is deprecated: use teleop_msgs-srv:GTParams-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <GTParams-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader teleop_msgs-srv:success-val is deprecated.  Use teleop_msgs-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GTParams-response>) ostream)
  "Serializes a message object of type '<GTParams-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GTParams-response>) istream)
  "Deserializes a message object of type '<GTParams-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GTParams-response>)))
  "Returns string type for a service object of type '<GTParams-response>"
  "teleop_msgs/GTParamsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GTParams-response)))
  "Returns string type for a service object of type 'GTParams-response"
  "teleop_msgs/GTParamsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GTParams-response>)))
  "Returns md5sum for a message object of type '<GTParams-response>"
  "2d7aed71a95980a2d156ffd0840bab20")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GTParams-response)))
  "Returns md5sum for a message object of type 'GTParams-response"
  "2d7aed71a95980a2d156ffd0840bab20")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GTParams-response>)))
  "Returns full string definition for message of type '<GTParams-response>"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GTParams-response)))
  "Returns full string definition for message of type 'GTParams-response"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GTParams-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GTParams-response>))
  "Converts a ROS message object to a list"
  (cl:list 'GTParams-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'GTParams)))
  'GTParams-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'GTParams)))
  'GTParams-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GTParams)))
  "Returns string type for a service object of type '<GTParams>"
  "teleop_msgs/GTParams")