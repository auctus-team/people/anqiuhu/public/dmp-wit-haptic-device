;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::InterestPose)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'InterestPose (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::INTERESTPOSE")
  (make-package "TELEOP_MSGS::INTERESTPOSE"))
(unless (find-package "TELEOP_MSGS::INTERESTPOSEREQUEST")
  (make-package "TELEOP_MSGS::INTERESTPOSEREQUEST"))
(unless (find-package "TELEOP_MSGS::INTERESTPOSERESPONSE")
  (make-package "TELEOP_MSGS::INTERESTPOSERESPONSE"))

(in-package "ROS")

(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))




(defclass teleop_msgs::InterestPoseRequest
  :super ros::object
  :slots (_interest_pose ))

(defmethod teleop_msgs::InterestPoseRequest
  (:init
   (&key
    ((:interest_pose __interest_pose) (instance geometry_msgs::PoseArray :init))
    )
   (send-super :init)
   (setq _interest_pose __interest_pose)
   self)
  (:interest_pose
   (&rest __interest_pose)
   (if (keywordp (car __interest_pose))
       (send* _interest_pose __interest_pose)
     (progn
       (if __interest_pose (setq _interest_pose (car __interest_pose)))
       _interest_pose)))
  (:serialization-length
   ()
   (+
    ;; geometry_msgs/PoseArray _interest_pose
    (send _interest_pose :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; geometry_msgs/PoseArray _interest_pose
       (send _interest_pose :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; geometry_msgs/PoseArray _interest_pose
     (send _interest_pose :deserialize buf ptr-) (incf ptr- (send _interest_pose :serialization-length))
   ;;
   self)
  )

(defclass teleop_msgs::InterestPoseResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::InterestPoseResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::InterestPose
  :super ros::object
  :slots ())

(setf (get teleop_msgs::InterestPose :md5sum-) "353b04f6dc21618f6bc1a71e1c6743a9")
(setf (get teleop_msgs::InterestPose :datatype-) "teleop_msgs/InterestPose")
(setf (get teleop_msgs::InterestPose :request) teleop_msgs::InterestPoseRequest)
(setf (get teleop_msgs::InterestPose :response) teleop_msgs::InterestPoseResponse)

(defmethod teleop_msgs::InterestPoseRequest
  (:response () (instance teleop_msgs::InterestPoseResponse :init)))

(setf (get teleop_msgs::InterestPoseRequest :md5sum-) "353b04f6dc21618f6bc1a71e1c6743a9")
(setf (get teleop_msgs::InterestPoseRequest :datatype-) "teleop_msgs/InterestPoseRequest")
(setf (get teleop_msgs::InterestPoseRequest :definition-)
      "geometry_msgs/PoseArray interest_pose

================================================================================
MSG: geometry_msgs/PoseArray
# An array of poses with a header for global reference.

Header header

Pose[] poses

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
---
bool success
")

(setf (get teleop_msgs::InterestPoseResponse :md5sum-) "353b04f6dc21618f6bc1a71e1c6743a9")
(setf (get teleop_msgs::InterestPoseResponse :datatype-) "teleop_msgs/InterestPoseResponse")
(setf (get teleop_msgs::InterestPoseResponse :definition-)
      "geometry_msgs/PoseArray interest_pose

================================================================================
MSG: geometry_msgs/PoseArray
# An array of poses with a header for global reference.

Header header

Pose[] poses

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w
---
bool success
")



(provide :teleop_msgs/InterestPose "353b04f6dc21618f6bc1a71e1c6743a9")


