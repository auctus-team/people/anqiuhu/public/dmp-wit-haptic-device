;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::SDGains)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'SDGains (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::SDGAINS")
  (make-package "TELEOP_MSGS::SDGAINS"))
(unless (find-package "TELEOP_MSGS::SDGAINSREQUEST")
  (make-package "TELEOP_MSGS::SDGAINSREQUEST"))
(unless (find-package "TELEOP_MSGS::SDGAINSRESPONSE")
  (make-package "TELEOP_MSGS::SDGAINSRESPONSE"))

(in-package "ROS")





(defclass teleop_msgs::SDGainsRequest
  :super ros::object
  :slots (_K_ac _B_ac ))

(defmethod teleop_msgs::SDGainsRequest
  (:init
   (&key
    ((:K_ac __K_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac __B_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _K_ac __K_ac)
   (setq _B_ac __B_ac)
   self)
  (:K_ac
   (&optional __K_ac)
   (if __K_ac (setq _K_ac __K_ac)) _K_ac)
  (:B_ac
   (&optional __B_ac)
   (if __B_ac (setq _B_ac __B_ac)) _B_ac)
  (:serialization-length
   ()
   (+
    ;; float64[] _K_ac
    (* 8    (length _K_ac)) 4
    ;; float64[] _B_ac
    (* 8    (length _B_ac)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _K_ac
     (write-long (length _K_ac) s)
     (dotimes (i (length _K_ac))
       (sys::poke (elt _K_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac
     (write-long (length _B_ac) s)
     (dotimes (i (length _B_ac))
       (sys::poke (elt _B_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _K_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass teleop_msgs::SDGainsResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::SDGainsResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::SDGains
  :super ros::object
  :slots ())

(setf (get teleop_msgs::SDGains :md5sum-) "da7436754c6eed502ac4bf2d5d8f6b56")
(setf (get teleop_msgs::SDGains :datatype-) "teleop_msgs/SDGains")
(setf (get teleop_msgs::SDGains :request) teleop_msgs::SDGainsRequest)
(setf (get teleop_msgs::SDGains :response) teleop_msgs::SDGainsResponse)

(defmethod teleop_msgs::SDGainsRequest
  (:response () (instance teleop_msgs::SDGainsResponse :init)))

(setf (get teleop_msgs::SDGainsRequest :md5sum-) "da7436754c6eed502ac4bf2d5d8f6b56")
(setf (get teleop_msgs::SDGainsRequest :datatype-) "teleop_msgs/SDGainsRequest")
(setf (get teleop_msgs::SDGainsRequest :definition-)
      "float64[] K_ac
float64[] B_ac
---
bool success
")

(setf (get teleop_msgs::SDGainsResponse :md5sum-) "da7436754c6eed502ac4bf2d5d8f6b56")
(setf (get teleop_msgs::SDGainsResponse :datatype-) "teleop_msgs/SDGainsResponse")
(setf (get teleop_msgs::SDGainsResponse :definition-)
      "float64[] K_ac
float64[] B_ac
---
bool success
")



(provide :teleop_msgs/SDGains "da7436754c6eed502ac4bf2d5d8f6b56")


