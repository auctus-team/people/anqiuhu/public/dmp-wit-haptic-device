;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::PFParams)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'PFParams (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::PFPARAMS")
  (make-package "TELEOP_MSGS::PFPARAMS"))
(unless (find-package "TELEOP_MSGS::PFPARAMSREQUEST")
  (make-package "TELEOP_MSGS::PFPARAMSREQUEST"))
(unless (find-package "TELEOP_MSGS::PFPARAMSRESPONSE")
  (make-package "TELEOP_MSGS::PFPARAMSRESPONSE"))

(in-package "ROS")





(defclass teleop_msgs::PFParamsRequest
  :super ros::object
  :slots (_d_ac _K_ac _B_ac _K_ac_obs _B_ac_obs ))

(defmethod teleop_msgs::PFParamsRequest
  (:init
   (&key
    ((:d_ac __d_ac) 0.0)
    ((:K_ac __K_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac __B_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:K_ac_obs __K_ac_obs) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac_obs __B_ac_obs) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _d_ac (float __d_ac))
   (setq _K_ac __K_ac)
   (setq _B_ac __B_ac)
   (setq _K_ac_obs __K_ac_obs)
   (setq _B_ac_obs __B_ac_obs)
   self)
  (:d_ac
   (&optional __d_ac)
   (if __d_ac (setq _d_ac __d_ac)) _d_ac)
  (:K_ac
   (&optional __K_ac)
   (if __K_ac (setq _K_ac __K_ac)) _K_ac)
  (:B_ac
   (&optional __B_ac)
   (if __B_ac (setq _B_ac __B_ac)) _B_ac)
  (:K_ac_obs
   (&optional __K_ac_obs)
   (if __K_ac_obs (setq _K_ac_obs __K_ac_obs)) _K_ac_obs)
  (:B_ac_obs
   (&optional __B_ac_obs)
   (if __B_ac_obs (setq _B_ac_obs __B_ac_obs)) _B_ac_obs)
  (:serialization-length
   ()
   (+
    ;; float64 _d_ac
    8
    ;; float64[] _K_ac
    (* 8    (length _K_ac)) 4
    ;; float64[] _B_ac
    (* 8    (length _B_ac)) 4
    ;; float64[] _K_ac_obs
    (* 8    (length _K_ac_obs)) 4
    ;; float64[] _B_ac_obs
    (* 8    (length _B_ac_obs)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _d_ac
       (sys::poke _d_ac (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64[] _K_ac
     (write-long (length _K_ac) s)
     (dotimes (i (length _K_ac))
       (sys::poke (elt _K_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac
     (write-long (length _B_ac) s)
     (dotimes (i (length _B_ac))
       (sys::poke (elt _B_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _K_ac_obs
     (write-long (length _K_ac_obs) s)
     (dotimes (i (length _K_ac_obs))
       (sys::poke (elt _K_ac_obs i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac_obs
     (write-long (length _B_ac_obs) s)
     (dotimes (i (length _B_ac_obs))
       (sys::poke (elt _B_ac_obs i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _d_ac
     (setq _d_ac (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64[] _K_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _K_ac_obs
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac_obs (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac_obs i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac_obs
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac_obs (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac_obs i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass teleop_msgs::PFParamsResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::PFParamsResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::PFParams
  :super ros::object
  :slots ())

(setf (get teleop_msgs::PFParams :md5sum-) "91b1675836b1c032fa924fa8b58285ec")
(setf (get teleop_msgs::PFParams :datatype-) "teleop_msgs/PFParams")
(setf (get teleop_msgs::PFParams :request) teleop_msgs::PFParamsRequest)
(setf (get teleop_msgs::PFParams :response) teleop_msgs::PFParamsResponse)

(defmethod teleop_msgs::PFParamsRequest
  (:response () (instance teleop_msgs::PFParamsResponse :init)))

(setf (get teleop_msgs::PFParamsRequest :md5sum-) "91b1675836b1c032fa924fa8b58285ec")
(setf (get teleop_msgs::PFParamsRequest :datatype-) "teleop_msgs/PFParamsRequest")
(setf (get teleop_msgs::PFParamsRequest :definition-)
      "float64 d_ac
float64[] K_ac
float64[] B_ac
float64[] K_ac_obs
float64[] B_ac_obs
---
bool success
")

(setf (get teleop_msgs::PFParamsResponse :md5sum-) "91b1675836b1c032fa924fa8b58285ec")
(setf (get teleop_msgs::PFParamsResponse :datatype-) "teleop_msgs/PFParamsResponse")
(setf (get teleop_msgs::PFParamsResponse :definition-)
      "float64 d_ac
float64[] K_ac
float64[] B_ac
float64[] K_ac_obs
float64[] B_ac_obs
---
bool success
")



(provide :teleop_msgs/PFParams "91b1675836b1c032fa924fa8b58285ec")


