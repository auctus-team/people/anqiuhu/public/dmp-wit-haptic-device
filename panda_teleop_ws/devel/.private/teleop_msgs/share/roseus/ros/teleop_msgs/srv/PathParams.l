;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::PathParams)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'PathParams (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::PATHPARAMS")
  (make-package "TELEOP_MSGS::PATHPARAMS"))
(unless (find-package "TELEOP_MSGS::PATHPARAMSREQUEST")
  (make-package "TELEOP_MSGS::PATHPARAMSREQUEST"))
(unless (find-package "TELEOP_MSGS::PATHPARAMSRESPONSE")
  (make-package "TELEOP_MSGS::PATHPARAMSRESPONSE"))

(in-package "ROS")





(defclass teleop_msgs::PathParamsRequest
  :super ros::object
  :slots (_path_step ))

(defmethod teleop_msgs::PathParamsRequest
  (:init
   (&key
    ((:path_step __path_step) 0)
    )
   (send-super :init)
   (setq _path_step (round __path_step))
   self)
  (:path_step
   (&optional __path_step)
   (if __path_step (setq _path_step __path_step)) _path_step)
  (:serialization-length
   ()
   (+
    ;; uint32 _path_step
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint32 _path_step
       (write-long _path_step s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint32 _path_step
     (setq _path_step (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass teleop_msgs::PathParamsResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::PathParamsResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::PathParams
  :super ros::object
  :slots ())

(setf (get teleop_msgs::PathParams :md5sum-) "5cd08abbeba16b0ad3a10ce92f788c97")
(setf (get teleop_msgs::PathParams :datatype-) "teleop_msgs/PathParams")
(setf (get teleop_msgs::PathParams :request) teleop_msgs::PathParamsRequest)
(setf (get teleop_msgs::PathParams :response) teleop_msgs::PathParamsResponse)

(defmethod teleop_msgs::PathParamsRequest
  (:response () (instance teleop_msgs::PathParamsResponse :init)))

(setf (get teleop_msgs::PathParamsRequest :md5sum-) "5cd08abbeba16b0ad3a10ce92f788c97")
(setf (get teleop_msgs::PathParamsRequest :datatype-) "teleop_msgs/PathParamsRequest")
(setf (get teleop_msgs::PathParamsRequest :definition-)
      "uint32 path_step
---
bool success
")

(setf (get teleop_msgs::PathParamsResponse :md5sum-) "5cd08abbeba16b0ad3a10ce92f788c97")
(setf (get teleop_msgs::PathParamsResponse :datatype-) "teleop_msgs/PathParamsResponse")
(setf (get teleop_msgs::PathParamsResponse :definition-)
      "uint32 path_step
---
bool success
")



(provide :teleop_msgs/PathParams "5cd08abbeba16b0ad3a10ce92f788c97")


