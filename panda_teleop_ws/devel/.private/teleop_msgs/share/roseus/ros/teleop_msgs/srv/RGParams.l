;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::RGParams)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'RGParams (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::RGPARAMS")
  (make-package "TELEOP_MSGS::RGPARAMS"))
(unless (find-package "TELEOP_MSGS::RGPARAMSREQUEST")
  (make-package "TELEOP_MSGS::RGPARAMSREQUEST"))
(unless (find-package "TELEOP_MSGS::RGPARAMSRESPONSE")
  (make-package "TELEOP_MSGS::RGPARAMSRESPONSE"))

(in-package "ROS")





(defclass teleop_msgs::RGParamsRequest
  :super ros::object
  :slots (_a _b _c _d_ac _K_ac _B_ac _d_ac_obs _K_ac_obs _B_ac_obs _ac_type ))

(defmethod teleop_msgs::RGParamsRequest
  (:init
   (&key
    ((:a __a) 0.0)
    ((:b __b) 0.0)
    ((:c __c) 0.0)
    ((:d_ac __d_ac) 0.0)
    ((:K_ac __K_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac __B_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:d_ac_obs __d_ac_obs) 0.0)
    ((:K_ac_obs __K_ac_obs) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac_obs __B_ac_obs) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:ac_type __ac_type) "")
    )
   (send-super :init)
   (setq _a (float __a))
   (setq _b (float __b))
   (setq _c (float __c))
   (setq _d_ac (float __d_ac))
   (setq _K_ac __K_ac)
   (setq _B_ac __B_ac)
   (setq _d_ac_obs (float __d_ac_obs))
   (setq _K_ac_obs __K_ac_obs)
   (setq _B_ac_obs __B_ac_obs)
   (setq _ac_type (string __ac_type))
   self)
  (:a
   (&optional __a)
   (if __a (setq _a __a)) _a)
  (:b
   (&optional __b)
   (if __b (setq _b __b)) _b)
  (:c
   (&optional __c)
   (if __c (setq _c __c)) _c)
  (:d_ac
   (&optional __d_ac)
   (if __d_ac (setq _d_ac __d_ac)) _d_ac)
  (:K_ac
   (&optional __K_ac)
   (if __K_ac (setq _K_ac __K_ac)) _K_ac)
  (:B_ac
   (&optional __B_ac)
   (if __B_ac (setq _B_ac __B_ac)) _B_ac)
  (:d_ac_obs
   (&optional __d_ac_obs)
   (if __d_ac_obs (setq _d_ac_obs __d_ac_obs)) _d_ac_obs)
  (:K_ac_obs
   (&optional __K_ac_obs)
   (if __K_ac_obs (setq _K_ac_obs __K_ac_obs)) _K_ac_obs)
  (:B_ac_obs
   (&optional __B_ac_obs)
   (if __B_ac_obs (setq _B_ac_obs __B_ac_obs)) _B_ac_obs)
  (:ac_type
   (&optional __ac_type)
   (if __ac_type (setq _ac_type __ac_type)) _ac_type)
  (:serialization-length
   ()
   (+
    ;; float64 _a
    8
    ;; float64 _b
    8
    ;; float64 _c
    8
    ;; float64 _d_ac
    8
    ;; float64[] _K_ac
    (* 8    (length _K_ac)) 4
    ;; float64[] _B_ac
    (* 8    (length _B_ac)) 4
    ;; float64 _d_ac_obs
    8
    ;; float64[] _K_ac_obs
    (* 8    (length _K_ac_obs)) 4
    ;; float64[] _B_ac_obs
    (* 8    (length _B_ac_obs)) 4
    ;; string _ac_type
    4 (length _ac_type)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _a
       (sys::poke _a (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _b
       (sys::poke _b (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _c
       (sys::poke _c (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _d_ac
       (sys::poke _d_ac (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64[] _K_ac
     (write-long (length _K_ac) s)
     (dotimes (i (length _K_ac))
       (sys::poke (elt _K_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac
     (write-long (length _B_ac) s)
     (dotimes (i (length _B_ac))
       (sys::poke (elt _B_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64 _d_ac_obs
       (sys::poke _d_ac_obs (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64[] _K_ac_obs
     (write-long (length _K_ac_obs) s)
     (dotimes (i (length _K_ac_obs))
       (sys::poke (elt _K_ac_obs i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac_obs
     (write-long (length _B_ac_obs) s)
     (dotimes (i (length _B_ac_obs))
       (sys::poke (elt _B_ac_obs i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; string _ac_type
       (write-long (length _ac_type) s) (princ _ac_type s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _a
     (setq _a (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _b
     (setq _b (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _c
     (setq _c (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _d_ac
     (setq _d_ac (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64[] _K_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64 _d_ac_obs
     (setq _d_ac_obs (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64[] _K_ac_obs
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac_obs (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac_obs i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac_obs
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac_obs (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac_obs i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; string _ac_type
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _ac_type (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass teleop_msgs::RGParamsResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::RGParamsResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::RGParams
  :super ros::object
  :slots ())

(setf (get teleop_msgs::RGParams :md5sum-) "344384821ca7b92c854595797bdc3b71")
(setf (get teleop_msgs::RGParams :datatype-) "teleop_msgs/RGParams")
(setf (get teleop_msgs::RGParams :request) teleop_msgs::RGParamsRequest)
(setf (get teleop_msgs::RGParams :response) teleop_msgs::RGParamsResponse)

(defmethod teleop_msgs::RGParamsRequest
  (:response () (instance teleop_msgs::RGParamsResponse :init)))

(setf (get teleop_msgs::RGParamsRequest :md5sum-) "344384821ca7b92c854595797bdc3b71")
(setf (get teleop_msgs::RGParamsRequest :datatype-) "teleop_msgs/RGParamsRequest")
(setf (get teleop_msgs::RGParamsRequest :definition-)
      "float64 a
float64 b
float64 c
float64 d_ac
float64[] K_ac
float64[] B_ac
float64 d_ac_obs
float64[] K_ac_obs
float64[] B_ac_obs
string ac_type
---
bool success
")

(setf (get teleop_msgs::RGParamsResponse :md5sum-) "344384821ca7b92c854595797bdc3b71")
(setf (get teleop_msgs::RGParamsResponse :datatype-) "teleop_msgs/RGParamsResponse")
(setf (get teleop_msgs::RGParamsResponse :definition-)
      "float64 a
float64 b
float64 c
float64 d_ac
float64[] K_ac
float64[] B_ac
float64 d_ac_obs
float64[] K_ac_obs
float64[] B_ac_obs
string ac_type
---
bool success
")



(provide :teleop_msgs/RGParams "344384821ca7b92c854595797bdc3b71")


