;; Auto-generated. Do not edit!


(when (boundp 'teleop_msgs::GTParams)
  (if (not (find-package "TELEOP_MSGS"))
    (make-package "TELEOP_MSGS"))
  (shadow 'GTParams (find-package "TELEOP_MSGS")))
(unless (find-package "TELEOP_MSGS::GTPARAMS")
  (make-package "TELEOP_MSGS::GTPARAMS"))
(unless (find-package "TELEOP_MSGS::GTPARAMSREQUEST")
  (make-package "TELEOP_MSGS::GTPARAMSREQUEST"))
(unless (find-package "TELEOP_MSGS::GTPARAMSRESPONSE")
  (make-package "TELEOP_MSGS::GTPARAMSRESPONSE"))

(in-package "ROS")





(defclass teleop_msgs::GTParamsRequest
  :super ros::object
  :slots (_d_ac _K_ac _B_ac ))

(defmethod teleop_msgs::GTParamsRequest
  (:init
   (&key
    ((:d_ac __d_ac) 0.0)
    ((:K_ac __K_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:B_ac __B_ac) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _d_ac (float __d_ac))
   (setq _K_ac __K_ac)
   (setq _B_ac __B_ac)
   self)
  (:d_ac
   (&optional __d_ac)
   (if __d_ac (setq _d_ac __d_ac)) _d_ac)
  (:K_ac
   (&optional __K_ac)
   (if __K_ac (setq _K_ac __K_ac)) _K_ac)
  (:B_ac
   (&optional __B_ac)
   (if __B_ac (setq _B_ac __B_ac)) _B_ac)
  (:serialization-length
   ()
   (+
    ;; float64 _d_ac
    8
    ;; float64[] _K_ac
    (* 8    (length _K_ac)) 4
    ;; float64[] _B_ac
    (* 8    (length _B_ac)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _d_ac
       (sys::poke _d_ac (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64[] _K_ac
     (write-long (length _K_ac) s)
     (dotimes (i (length _K_ac))
       (sys::poke (elt _K_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _B_ac
     (write-long (length _B_ac) s)
     (dotimes (i (length _B_ac))
       (sys::poke (elt _B_ac i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _d_ac
     (setq _d_ac (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64[] _K_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _K_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _K_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _B_ac
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _B_ac (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _B_ac i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(defclass teleop_msgs::GTParamsResponse
  :super ros::object
  :slots (_success ))

(defmethod teleop_msgs::GTParamsResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass teleop_msgs::GTParams
  :super ros::object
  :slots ())

(setf (get teleop_msgs::GTParams :md5sum-) "2d7aed71a95980a2d156ffd0840bab20")
(setf (get teleop_msgs::GTParams :datatype-) "teleop_msgs/GTParams")
(setf (get teleop_msgs::GTParams :request) teleop_msgs::GTParamsRequest)
(setf (get teleop_msgs::GTParams :response) teleop_msgs::GTParamsResponse)

(defmethod teleop_msgs::GTParamsRequest
  (:response () (instance teleop_msgs::GTParamsResponse :init)))

(setf (get teleop_msgs::GTParamsRequest :md5sum-) "2d7aed71a95980a2d156ffd0840bab20")
(setf (get teleop_msgs::GTParamsRequest :datatype-) "teleop_msgs/GTParamsRequest")
(setf (get teleop_msgs::GTParamsRequest :definition-)
      "float64 d_ac
float64[] K_ac
float64[] B_ac
---
bool success
")

(setf (get teleop_msgs::GTParamsResponse :md5sum-) "2d7aed71a95980a2d156ffd0840bab20")
(setf (get teleop_msgs::GTParamsResponse :datatype-) "teleop_msgs/GTParamsResponse")
(setf (get teleop_msgs::GTParamsResponse :definition-)
      "float64 d_ac
float64[] K_ac
float64[] B_ac
---
bool success
")



(provide :teleop_msgs/GTParams "2d7aed71a95980a2d156ffd0840bab20")


