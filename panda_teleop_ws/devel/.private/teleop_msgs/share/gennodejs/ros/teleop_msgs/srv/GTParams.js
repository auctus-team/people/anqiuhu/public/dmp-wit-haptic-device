// Auto-generated. Do not edit!

// (in-package teleop_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class GTParamsRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.d_ac = null;
      this.K_ac = null;
      this.B_ac = null;
    }
    else {
      if (initObj.hasOwnProperty('d_ac')) {
        this.d_ac = initObj.d_ac
      }
      else {
        this.d_ac = 0.0;
      }
      if (initObj.hasOwnProperty('K_ac')) {
        this.K_ac = initObj.K_ac
      }
      else {
        this.K_ac = [];
      }
      if (initObj.hasOwnProperty('B_ac')) {
        this.B_ac = initObj.B_ac
      }
      else {
        this.B_ac = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GTParamsRequest
    // Serialize message field [d_ac]
    bufferOffset = _serializer.float64(obj.d_ac, buffer, bufferOffset);
    // Serialize message field [K_ac]
    bufferOffset = _arraySerializer.float64(obj.K_ac, buffer, bufferOffset, null);
    // Serialize message field [B_ac]
    bufferOffset = _arraySerializer.float64(obj.B_ac, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GTParamsRequest
    let len;
    let data = new GTParamsRequest(null);
    // Deserialize message field [d_ac]
    data.d_ac = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [K_ac]
    data.K_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [B_ac]
    data.B_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.K_ac.length;
    length += 8 * object.B_ac.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/GTParamsRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '014b0389b0a3f54b787ab0a0083a4b49';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 d_ac
    float64[] K_ac
    float64[] B_ac
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GTParamsRequest(null);
    if (msg.d_ac !== undefined) {
      resolved.d_ac = msg.d_ac;
    }
    else {
      resolved.d_ac = 0.0
    }

    if (msg.K_ac !== undefined) {
      resolved.K_ac = msg.K_ac;
    }
    else {
      resolved.K_ac = []
    }

    if (msg.B_ac !== undefined) {
      resolved.B_ac = msg.B_ac;
    }
    else {
      resolved.B_ac = []
    }

    return resolved;
    }
};

class GTParamsResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GTParamsResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GTParamsResponse
    let len;
    let data = new GTParamsResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/GTParamsResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '358e233cde0c8a8bcfea4ce193f8fc15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GTParamsResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    return resolved;
    }
};

module.exports = {
  Request: GTParamsRequest,
  Response: GTParamsResponse,
  md5sum() { return '2d7aed71a95980a2d156ffd0840bab20'; },
  datatype() { return 'teleop_msgs/GTParams'; }
};
