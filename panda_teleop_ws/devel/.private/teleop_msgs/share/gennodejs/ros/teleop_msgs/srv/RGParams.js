// Auto-generated. Do not edit!

// (in-package teleop_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class RGParamsRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.a = null;
      this.b = null;
      this.c = null;
      this.d_ac = null;
      this.K_ac = null;
      this.B_ac = null;
      this.d_ac_obs = null;
      this.K_ac_obs = null;
      this.B_ac_obs = null;
      this.ac_type = null;
    }
    else {
      if (initObj.hasOwnProperty('a')) {
        this.a = initObj.a
      }
      else {
        this.a = 0.0;
      }
      if (initObj.hasOwnProperty('b')) {
        this.b = initObj.b
      }
      else {
        this.b = 0.0;
      }
      if (initObj.hasOwnProperty('c')) {
        this.c = initObj.c
      }
      else {
        this.c = 0.0;
      }
      if (initObj.hasOwnProperty('d_ac')) {
        this.d_ac = initObj.d_ac
      }
      else {
        this.d_ac = 0.0;
      }
      if (initObj.hasOwnProperty('K_ac')) {
        this.K_ac = initObj.K_ac
      }
      else {
        this.K_ac = [];
      }
      if (initObj.hasOwnProperty('B_ac')) {
        this.B_ac = initObj.B_ac
      }
      else {
        this.B_ac = [];
      }
      if (initObj.hasOwnProperty('d_ac_obs')) {
        this.d_ac_obs = initObj.d_ac_obs
      }
      else {
        this.d_ac_obs = 0.0;
      }
      if (initObj.hasOwnProperty('K_ac_obs')) {
        this.K_ac_obs = initObj.K_ac_obs
      }
      else {
        this.K_ac_obs = [];
      }
      if (initObj.hasOwnProperty('B_ac_obs')) {
        this.B_ac_obs = initObj.B_ac_obs
      }
      else {
        this.B_ac_obs = [];
      }
      if (initObj.hasOwnProperty('ac_type')) {
        this.ac_type = initObj.ac_type
      }
      else {
        this.ac_type = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RGParamsRequest
    // Serialize message field [a]
    bufferOffset = _serializer.float64(obj.a, buffer, bufferOffset);
    // Serialize message field [b]
    bufferOffset = _serializer.float64(obj.b, buffer, bufferOffset);
    // Serialize message field [c]
    bufferOffset = _serializer.float64(obj.c, buffer, bufferOffset);
    // Serialize message field [d_ac]
    bufferOffset = _serializer.float64(obj.d_ac, buffer, bufferOffset);
    // Serialize message field [K_ac]
    bufferOffset = _arraySerializer.float64(obj.K_ac, buffer, bufferOffset, null);
    // Serialize message field [B_ac]
    bufferOffset = _arraySerializer.float64(obj.B_ac, buffer, bufferOffset, null);
    // Serialize message field [d_ac_obs]
    bufferOffset = _serializer.float64(obj.d_ac_obs, buffer, bufferOffset);
    // Serialize message field [K_ac_obs]
    bufferOffset = _arraySerializer.float64(obj.K_ac_obs, buffer, bufferOffset, null);
    // Serialize message field [B_ac_obs]
    bufferOffset = _arraySerializer.float64(obj.B_ac_obs, buffer, bufferOffset, null);
    // Serialize message field [ac_type]
    bufferOffset = _serializer.string(obj.ac_type, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RGParamsRequest
    let len;
    let data = new RGParamsRequest(null);
    // Deserialize message field [a]
    data.a = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [b]
    data.b = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [c]
    data.c = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [d_ac]
    data.d_ac = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [K_ac]
    data.K_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [B_ac]
    data.B_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [d_ac_obs]
    data.d_ac_obs = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [K_ac_obs]
    data.K_ac_obs = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [B_ac_obs]
    data.B_ac_obs = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [ac_type]
    data.ac_type = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.K_ac.length;
    length += 8 * object.B_ac.length;
    length += 8 * object.K_ac_obs.length;
    length += 8 * object.B_ac_obs.length;
    length += _getByteLength(object.ac_type);
    return length + 60;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/RGParamsRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '4f3d278473d74edb9e80f5f56c06580b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 a
    float64 b
    float64 c
    float64 d_ac
    float64[] K_ac
    float64[] B_ac
    float64 d_ac_obs
    float64[] K_ac_obs
    float64[] B_ac_obs
    string ac_type
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RGParamsRequest(null);
    if (msg.a !== undefined) {
      resolved.a = msg.a;
    }
    else {
      resolved.a = 0.0
    }

    if (msg.b !== undefined) {
      resolved.b = msg.b;
    }
    else {
      resolved.b = 0.0
    }

    if (msg.c !== undefined) {
      resolved.c = msg.c;
    }
    else {
      resolved.c = 0.0
    }

    if (msg.d_ac !== undefined) {
      resolved.d_ac = msg.d_ac;
    }
    else {
      resolved.d_ac = 0.0
    }

    if (msg.K_ac !== undefined) {
      resolved.K_ac = msg.K_ac;
    }
    else {
      resolved.K_ac = []
    }

    if (msg.B_ac !== undefined) {
      resolved.B_ac = msg.B_ac;
    }
    else {
      resolved.B_ac = []
    }

    if (msg.d_ac_obs !== undefined) {
      resolved.d_ac_obs = msg.d_ac_obs;
    }
    else {
      resolved.d_ac_obs = 0.0
    }

    if (msg.K_ac_obs !== undefined) {
      resolved.K_ac_obs = msg.K_ac_obs;
    }
    else {
      resolved.K_ac_obs = []
    }

    if (msg.B_ac_obs !== undefined) {
      resolved.B_ac_obs = msg.B_ac_obs;
    }
    else {
      resolved.B_ac_obs = []
    }

    if (msg.ac_type !== undefined) {
      resolved.ac_type = msg.ac_type;
    }
    else {
      resolved.ac_type = ''
    }

    return resolved;
    }
};

class RGParamsResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RGParamsResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RGParamsResponse
    let len;
    let data = new RGParamsResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/RGParamsResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '358e233cde0c8a8bcfea4ce193f8fc15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RGParamsResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    return resolved;
    }
};

module.exports = {
  Request: RGParamsRequest,
  Response: RGParamsResponse,
  md5sum() { return '344384821ca7b92c854595797bdc3b71'; },
  datatype() { return 'teleop_msgs/RGParams'; }
};
