// Auto-generated. Do not edit!

// (in-package teleop_msgs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class SDGainsRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.K_ac = null;
      this.B_ac = null;
    }
    else {
      if (initObj.hasOwnProperty('K_ac')) {
        this.K_ac = initObj.K_ac
      }
      else {
        this.K_ac = [];
      }
      if (initObj.hasOwnProperty('B_ac')) {
        this.B_ac = initObj.B_ac
      }
      else {
        this.B_ac = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SDGainsRequest
    // Serialize message field [K_ac]
    bufferOffset = _arraySerializer.float64(obj.K_ac, buffer, bufferOffset, null);
    // Serialize message field [B_ac]
    bufferOffset = _arraySerializer.float64(obj.B_ac, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SDGainsRequest
    let len;
    let data = new SDGainsRequest(null);
    // Deserialize message field [K_ac]
    data.K_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [B_ac]
    data.B_ac = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.K_ac.length;
    length += 8 * object.B_ac.length;
    return length + 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/SDGainsRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '76e296724549f8e32a2266f34b3b5d2b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] K_ac
    float64[] B_ac
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SDGainsRequest(null);
    if (msg.K_ac !== undefined) {
      resolved.K_ac = msg.K_ac;
    }
    else {
      resolved.K_ac = []
    }

    if (msg.B_ac !== undefined) {
      resolved.B_ac = msg.B_ac;
    }
    else {
      resolved.B_ac = []
    }

    return resolved;
    }
};

class SDGainsResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.success = null;
    }
    else {
      if (initObj.hasOwnProperty('success')) {
        this.success = initObj.success
      }
      else {
        this.success = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type SDGainsResponse
    // Serialize message field [success]
    bufferOffset = _serializer.bool(obj.success, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type SDGainsResponse
    let len;
    let data = new SDGainsResponse(null);
    // Deserialize message field [success]
    data.success = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a service object
    return 'teleop_msgs/SDGainsResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '358e233cde0c8a8bcfea4ce193f8fc15';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool success
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new SDGainsResponse(null);
    if (msg.success !== undefined) {
      resolved.success = msg.success;
    }
    else {
      resolved.success = false
    }

    return resolved;
    }
};

module.exports = {
  Request: SDGainsRequest,
  Response: SDGainsResponse,
  md5sum() { return 'da7436754c6eed502ac4bf2d5d8f6b56'; },
  datatype() { return 'teleop_msgs/SDGains'; }
};
