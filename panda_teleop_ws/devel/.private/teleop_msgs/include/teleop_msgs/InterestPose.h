// Generated by gencpp from file teleop_msgs/InterestPose.msg
// DO NOT EDIT!


#ifndef TELEOP_MSGS_MESSAGE_INTERESTPOSE_H
#define TELEOP_MSGS_MESSAGE_INTERESTPOSE_H

#include <ros/service_traits.h>


#include <teleop_msgs/InterestPoseRequest.h>
#include <teleop_msgs/InterestPoseResponse.h>


namespace teleop_msgs
{

struct InterestPose
{

typedef InterestPoseRequest Request;
typedef InterestPoseResponse Response;
Request request;
Response response;

typedef Request RequestType;
typedef Response ResponseType;

}; // struct InterestPose
} // namespace teleop_msgs


namespace ros
{
namespace service_traits
{


template<>
struct MD5Sum< ::teleop_msgs::InterestPose > {
  static const char* value()
  {
    return "353b04f6dc21618f6bc1a71e1c6743a9";
  }

  static const char* value(const ::teleop_msgs::InterestPose&) { return value(); }
};

template<>
struct DataType< ::teleop_msgs::InterestPose > {
  static const char* value()
  {
    return "teleop_msgs/InterestPose";
  }

  static const char* value(const ::teleop_msgs::InterestPose&) { return value(); }
};


// service_traits::MD5Sum< ::teleop_msgs::InterestPoseRequest> should match
// service_traits::MD5Sum< ::teleop_msgs::InterestPose >
template<>
struct MD5Sum< ::teleop_msgs::InterestPoseRequest>
{
  static const char* value()
  {
    return MD5Sum< ::teleop_msgs::InterestPose >::value();
  }
  static const char* value(const ::teleop_msgs::InterestPoseRequest&)
  {
    return value();
  }
};

// service_traits::DataType< ::teleop_msgs::InterestPoseRequest> should match
// service_traits::DataType< ::teleop_msgs::InterestPose >
template<>
struct DataType< ::teleop_msgs::InterestPoseRequest>
{
  static const char* value()
  {
    return DataType< ::teleop_msgs::InterestPose >::value();
  }
  static const char* value(const ::teleop_msgs::InterestPoseRequest&)
  {
    return value();
  }
};

// service_traits::MD5Sum< ::teleop_msgs::InterestPoseResponse> should match
// service_traits::MD5Sum< ::teleop_msgs::InterestPose >
template<>
struct MD5Sum< ::teleop_msgs::InterestPoseResponse>
{
  static const char* value()
  {
    return MD5Sum< ::teleop_msgs::InterestPose >::value();
  }
  static const char* value(const ::teleop_msgs::InterestPoseResponse&)
  {
    return value();
  }
};

// service_traits::DataType< ::teleop_msgs::InterestPoseResponse> should match
// service_traits::DataType< ::teleop_msgs::InterestPose >
template<>
struct DataType< ::teleop_msgs::InterestPoseResponse>
{
  static const char* value()
  {
    return DataType< ::teleop_msgs::InterestPose >::value();
  }
  static const char* value(const ::teleop_msgs::InterestPoseResponse&)
  {
    return value();
  }
};

} // namespace service_traits
} // namespace ros

#endif // TELEOP_MSGS_MESSAGE_INTERESTPOSE_H
