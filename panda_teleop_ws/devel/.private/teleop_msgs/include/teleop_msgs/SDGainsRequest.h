// Generated by gencpp from file teleop_msgs/SDGainsRequest.msg
// DO NOT EDIT!


#ifndef TELEOP_MSGS_MESSAGE_SDGAINSREQUEST_H
#define TELEOP_MSGS_MESSAGE_SDGAINSREQUEST_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace teleop_msgs
{
template <class ContainerAllocator>
struct SDGainsRequest_
{
  typedef SDGainsRequest_<ContainerAllocator> Type;

  SDGainsRequest_()
    : K_ac()
    , B_ac()  {
    }
  SDGainsRequest_(const ContainerAllocator& _alloc)
    : K_ac(_alloc)
    , B_ac(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector<double, typename std::allocator_traits<ContainerAllocator>::template rebind_alloc<double>> _K_ac_type;
  _K_ac_type K_ac;

   typedef std::vector<double, typename std::allocator_traits<ContainerAllocator>::template rebind_alloc<double>> _B_ac_type;
  _B_ac_type B_ac;





  typedef boost::shared_ptr< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> const> ConstPtr;

}; // struct SDGainsRequest_

typedef ::teleop_msgs::SDGainsRequest_<std::allocator<void> > SDGainsRequest;

typedef boost::shared_ptr< ::teleop_msgs::SDGainsRequest > SDGainsRequestPtr;
typedef boost::shared_ptr< ::teleop_msgs::SDGainsRequest const> SDGainsRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::teleop_msgs::SDGainsRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::teleop_msgs::SDGainsRequest_<ContainerAllocator1> & lhs, const ::teleop_msgs::SDGainsRequest_<ContainerAllocator2> & rhs)
{
  return lhs.K_ac == rhs.K_ac &&
    lhs.B_ac == rhs.B_ac;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::teleop_msgs::SDGainsRequest_<ContainerAllocator1> & lhs, const ::teleop_msgs::SDGainsRequest_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace teleop_msgs

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "76e296724549f8e32a2266f34b3b5d2b";
  }

  static const char* value(const ::teleop_msgs::SDGainsRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x76e296724549f8e3ULL;
  static const uint64_t static_value2 = 0x2a2266f34b3b5d2bULL;
};

template<class ContainerAllocator>
struct DataType< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "teleop_msgs/SDGainsRequest";
  }

  static const char* value(const ::teleop_msgs::SDGainsRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float64[] K_ac\n"
"float64[] B_ac\n"
;
  }

  static const char* value(const ::teleop_msgs::SDGainsRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.K_ac);
      stream.next(m.B_ac);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct SDGainsRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::teleop_msgs::SDGainsRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::teleop_msgs::SDGainsRequest_<ContainerAllocator>& v)
  {
    s << indent << "K_ac[]" << std::endl;
    for (size_t i = 0; i < v.K_ac.size(); ++i)
    {
      s << indent << "  K_ac[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.K_ac[i]);
    }
    s << indent << "B_ac[]" << std::endl;
    for (size_t i = 0; i < v.B_ac.size(); ++i)
    {
      s << indent << "  B_ac[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.B_ac[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // TELEOP_MSGS_MESSAGE_SDGAINSREQUEST_H
