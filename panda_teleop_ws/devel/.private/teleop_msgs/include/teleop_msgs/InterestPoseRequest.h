// Generated by gencpp from file teleop_msgs/InterestPoseRequest.msg
// DO NOT EDIT!


#ifndef TELEOP_MSGS_MESSAGE_INTERESTPOSEREQUEST_H
#define TELEOP_MSGS_MESSAGE_INTERESTPOSEREQUEST_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <geometry_msgs/PoseArray.h>

namespace teleop_msgs
{
template <class ContainerAllocator>
struct InterestPoseRequest_
{
  typedef InterestPoseRequest_<ContainerAllocator> Type;

  InterestPoseRequest_()
    : interest_pose()  {
    }
  InterestPoseRequest_(const ContainerAllocator& _alloc)
    : interest_pose(_alloc)  {
  (void)_alloc;
    }



   typedef  ::geometry_msgs::PoseArray_<ContainerAllocator>  _interest_pose_type;
  _interest_pose_type interest_pose;





  typedef boost::shared_ptr< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> const> ConstPtr;

}; // struct InterestPoseRequest_

typedef ::teleop_msgs::InterestPoseRequest_<std::allocator<void> > InterestPoseRequest;

typedef boost::shared_ptr< ::teleop_msgs::InterestPoseRequest > InterestPoseRequestPtr;
typedef boost::shared_ptr< ::teleop_msgs::InterestPoseRequest const> InterestPoseRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator1> & lhs, const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator2> & rhs)
{
  return lhs.interest_pose == rhs.interest_pose;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator1> & lhs, const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace teleop_msgs

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "8dc1973ee01e9a757e90ea68520244f5";
  }

  static const char* value(const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x8dc1973ee01e9a75ULL;
  static const uint64_t static_value2 = 0x7e90ea68520244f5ULL;
};

template<class ContainerAllocator>
struct DataType< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "teleop_msgs/InterestPoseRequest";
  }

  static const char* value(const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "geometry_msgs/PoseArray interest_pose\n"
"\n"
"================================================================================\n"
"MSG: geometry_msgs/PoseArray\n"
"# An array of poses with a header for global reference.\n"
"\n"
"Header header\n"
"\n"
"Pose[] poses\n"
"\n"
"================================================================================\n"
"MSG: std_msgs/Header\n"
"# Standard metadata for higher-level stamped data types.\n"
"# This is generally used to communicate timestamped data \n"
"# in a particular coordinate frame.\n"
"# \n"
"# sequence ID: consecutively increasing ID \n"
"uint32 seq\n"
"#Two-integer timestamp that is expressed as:\n"
"# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n"
"# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n"
"# time-handling sugar is provided by the client library\n"
"time stamp\n"
"#Frame this data is associated with\n"
"string frame_id\n"
"\n"
"================================================================================\n"
"MSG: geometry_msgs/Pose\n"
"# A representation of pose in free space, composed of position and orientation. \n"
"Point position\n"
"Quaternion orientation\n"
"\n"
"================================================================================\n"
"MSG: geometry_msgs/Point\n"
"# This contains the position of a point in free space\n"
"float64 x\n"
"float64 y\n"
"float64 z\n"
"\n"
"================================================================================\n"
"MSG: geometry_msgs/Quaternion\n"
"# This represents an orientation in free space in quaternion form.\n"
"\n"
"float64 x\n"
"float64 y\n"
"float64 z\n"
"float64 w\n"
;
  }

  static const char* value(const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.interest_pose);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct InterestPoseRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::teleop_msgs::InterestPoseRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::teleop_msgs::InterestPoseRequest_<ContainerAllocator>& v)
  {
    s << indent << "interest_pose: ";
    s << std::endl;
    Printer< ::geometry_msgs::PoseArray_<ContainerAllocator> >::stream(s, indent + "  ", v.interest_pose);
  }
};

} // namespace message_operations
} // namespace ros

#endif // TELEOP_MSGS_MESSAGE_INTERESTPOSEREQUEST_H
