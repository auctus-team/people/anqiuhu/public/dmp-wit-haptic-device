;; Auto-generated. Do not edit!


(when (boundp 'panda_qp_control::PandaRunMsg)
  (if (not (find-package "PANDA_QP_CONTROL"))
    (make-package "PANDA_QP_CONTROL"))
  (shadow 'PandaRunMsg (find-package "PANDA_QP_CONTROL")))
(unless (find-package "PANDA_QP_CONTROL::PANDARUNMSG")
  (make-package "PANDA_QP_CONTROL::PANDARUNMSG"))

(in-package "ROS")
;;//! \htmlinclude PandaRunMsg.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass panda_qp_control::PandaRunMsg
  :super ros::object
  :slots (_header _X_err ))

(defmethod panda_qp_control::PandaRunMsg
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:X_err __X_err) (instance geometry_msgs::Twist :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _X_err __X_err)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:X_err
   (&rest __X_err)
   (if (keywordp (car __X_err))
       (send* _X_err __X_err)
     (progn
       (if __X_err (setq _X_err (car __X_err)))
       _X_err)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Twist _X_err
    (send _X_err :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Twist _X_err
       (send _X_err :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Twist _X_err
     (send _X_err :deserialize buf ptr-) (incf ptr- (send _X_err :serialization-length))
   ;;
   self)
  )

(setf (get panda_qp_control::PandaRunMsg :md5sum-) "8d02e4fbb1497f25981026af4efe65e3")
(setf (get panda_qp_control::PandaRunMsg :datatype-) "panda_qp_control/PandaRunMsg")
(setf (get panda_qp_control::PandaRunMsg :definition-)
      "Header header
geometry_msgs/Twist X_err

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :panda_qp_control/PandaRunMsg "8d02e4fbb1497f25981026af4efe65e3")


