; Auto-generated. Do not edit!


(cl:in-package panda_qp_control-msg)


;//! \htmlinclude PandaRunMsg.msg.html

(cl:defclass <PandaRunMsg> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (X_err
    :reader X_err
    :initarg :X_err
    :type geometry_msgs-msg:Twist
    :initform (cl:make-instance 'geometry_msgs-msg:Twist)))
)

(cl:defclass PandaRunMsg (<PandaRunMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PandaRunMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PandaRunMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name panda_qp_control-msg:<PandaRunMsg> is deprecated: use panda_qp_control-msg:PandaRunMsg instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PandaRunMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader panda_qp_control-msg:header-val is deprecated.  Use panda_qp_control-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'X_err-val :lambda-list '(m))
(cl:defmethod X_err-val ((m <PandaRunMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader panda_qp_control-msg:X_err-val is deprecated.  Use panda_qp_control-msg:X_err instead.")
  (X_err m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PandaRunMsg>) ostream)
  "Serializes a message object of type '<PandaRunMsg>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'X_err) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PandaRunMsg>) istream)
  "Deserializes a message object of type '<PandaRunMsg>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'X_err) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PandaRunMsg>)))
  "Returns string type for a message object of type '<PandaRunMsg>"
  "panda_qp_control/PandaRunMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PandaRunMsg)))
  "Returns string type for a message object of type 'PandaRunMsg"
  "panda_qp_control/PandaRunMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PandaRunMsg>)))
  "Returns md5sum for a message object of type '<PandaRunMsg>"
  "8d02e4fbb1497f25981026af4efe65e3")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PandaRunMsg)))
  "Returns md5sum for a message object of type 'PandaRunMsg"
  "8d02e4fbb1497f25981026af4efe65e3")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PandaRunMsg>)))
  "Returns full string definition for message of type '<PandaRunMsg>"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist X_err~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PandaRunMsg)))
  "Returns full string definition for message of type 'PandaRunMsg"
  (cl:format cl:nil "Header header~%geometry_msgs/Twist X_err~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PandaRunMsg>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'X_err))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PandaRunMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'PandaRunMsg
    (cl:cons ':header (header msg))
    (cl:cons ':X_err (X_err msg))
))
