; Auto-generated. Do not edit!


(cl:in-package moveit_trajectory_interface-srv)


;//! \htmlinclude Follow-request.msg.html

(cl:defclass <Follow-request> (roslisp-msg-protocol:ros-message)
  ((topic
    :reader topic
    :initarg :topic
    :type cl:string
    :initform "")
   (flag
    :reader flag
    :initarg :flag
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Follow-request (<Follow-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Follow-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Follow-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name moveit_trajectory_interface-srv:<Follow-request> is deprecated: use moveit_trajectory_interface-srv:Follow-request instead.")))

(cl:ensure-generic-function 'topic-val :lambda-list '(m))
(cl:defmethod topic-val ((m <Follow-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader moveit_trajectory_interface-srv:topic-val is deprecated.  Use moveit_trajectory_interface-srv:topic instead.")
  (topic m))

(cl:ensure-generic-function 'flag-val :lambda-list '(m))
(cl:defmethod flag-val ((m <Follow-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader moveit_trajectory_interface-srv:flag-val is deprecated.  Use moveit_trajectory_interface-srv:flag instead.")
  (flag m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Follow-request>) ostream)
  "Serializes a message object of type '<Follow-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'topic))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'topic))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'flag) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Follow-request>) istream)
  "Deserializes a message object of type '<Follow-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'topic) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'topic) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'flag) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Follow-request>)))
  "Returns string type for a service object of type '<Follow-request>"
  "moveit_trajectory_interface/FollowRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Follow-request)))
  "Returns string type for a service object of type 'Follow-request"
  "moveit_trajectory_interface/FollowRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Follow-request>)))
  "Returns md5sum for a message object of type '<Follow-request>"
  "3d18b803936c9bc6d13b57b51b92d129")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Follow-request)))
  "Returns md5sum for a message object of type 'Follow-request"
  "3d18b803936c9bc6d13b57b51b92d129")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Follow-request>)))
  "Returns full string definition for message of type '<Follow-request>"
  (cl:format cl:nil "string topic~%bool flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Follow-request)))
  "Returns full string definition for message of type 'Follow-request"
  (cl:format cl:nil "string topic~%bool flag~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Follow-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'topic))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Follow-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Follow-request
    (cl:cons ':topic (topic msg))
    (cl:cons ':flag (flag msg))
))
;//! \htmlinclude Follow-response.msg.html

(cl:defclass <Follow-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass Follow-response (<Follow-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Follow-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Follow-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name moveit_trajectory_interface-srv:<Follow-response> is deprecated: use moveit_trajectory_interface-srv:Follow-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Follow-response>) ostream)
  "Serializes a message object of type '<Follow-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Follow-response>) istream)
  "Deserializes a message object of type '<Follow-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Follow-response>)))
  "Returns string type for a service object of type '<Follow-response>"
  "moveit_trajectory_interface/FollowResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Follow-response)))
  "Returns string type for a service object of type 'Follow-response"
  "moveit_trajectory_interface/FollowResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Follow-response>)))
  "Returns md5sum for a message object of type '<Follow-response>"
  "3d18b803936c9bc6d13b57b51b92d129")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Follow-response)))
  "Returns md5sum for a message object of type 'Follow-response"
  "3d18b803936c9bc6d13b57b51b92d129")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Follow-response>)))
  "Returns full string definition for message of type '<Follow-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Follow-response)))
  "Returns full string definition for message of type 'Follow-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Follow-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Follow-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Follow-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Follow)))
  'Follow-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Follow)))
  'Follow-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Follow)))
  "Returns string type for a service object of type '<Follow>"
  "moveit_trajectory_interface/Follow")