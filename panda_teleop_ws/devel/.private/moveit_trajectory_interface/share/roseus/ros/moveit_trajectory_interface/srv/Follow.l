;; Auto-generated. Do not edit!


(when (boundp 'moveit_trajectory_interface::Follow)
  (if (not (find-package "MOVEIT_TRAJECTORY_INTERFACE"))
    (make-package "MOVEIT_TRAJECTORY_INTERFACE"))
  (shadow 'Follow (find-package "MOVEIT_TRAJECTORY_INTERFACE")))
(unless (find-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOW")
  (make-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOW"))
(unless (find-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOWREQUEST")
  (make-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOWREQUEST"))
(unless (find-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOWRESPONSE")
  (make-package "MOVEIT_TRAJECTORY_INTERFACE::FOLLOWRESPONSE"))

(in-package "ROS")





(defclass moveit_trajectory_interface::FollowRequest
  :super ros::object
  :slots (_topic _flag ))

(defmethod moveit_trajectory_interface::FollowRequest
  (:init
   (&key
    ((:topic __topic) "")
    ((:flag __flag) nil)
    )
   (send-super :init)
   (setq _topic (string __topic))
   (setq _flag __flag)
   self)
  (:topic
   (&optional __topic)
   (if __topic (setq _topic __topic)) _topic)
  (:flag
   (&optional (__flag :null))
   (if (not (eq __flag :null)) (setq _flag __flag)) _flag)
  (:serialization-length
   ()
   (+
    ;; string _topic
    4 (length _topic)
    ;; bool _flag
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _topic
       (write-long (length _topic) s) (princ _topic s)
     ;; bool _flag
       (if _flag (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _topic
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _topic (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _flag
     (setq _flag (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass moveit_trajectory_interface::FollowResponse
  :super ros::object
  :slots ())

(defmethod moveit_trajectory_interface::FollowResponse
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass moveit_trajectory_interface::Follow
  :super ros::object
  :slots ())

(setf (get moveit_trajectory_interface::Follow :md5sum-) "3d18b803936c9bc6d13b57b51b92d129")
(setf (get moveit_trajectory_interface::Follow :datatype-) "moveit_trajectory_interface/Follow")
(setf (get moveit_trajectory_interface::Follow :request) moveit_trajectory_interface::FollowRequest)
(setf (get moveit_trajectory_interface::Follow :response) moveit_trajectory_interface::FollowResponse)

(defmethod moveit_trajectory_interface::FollowRequest
  (:response () (instance moveit_trajectory_interface::FollowResponse :init)))

(setf (get moveit_trajectory_interface::FollowRequest :md5sum-) "3d18b803936c9bc6d13b57b51b92d129")
(setf (get moveit_trajectory_interface::FollowRequest :datatype-) "moveit_trajectory_interface/FollowRequest")
(setf (get moveit_trajectory_interface::FollowRequest :definition-)
      "string topic
bool flag
---

")

(setf (get moveit_trajectory_interface::FollowResponse :md5sum-) "3d18b803936c9bc6d13b57b51b92d129")
(setf (get moveit_trajectory_interface::FollowResponse :datatype-) "moveit_trajectory_interface/FollowResponse")
(setf (get moveit_trajectory_interface::FollowResponse :definition-)
      "string topic
bool flag
---

")



(provide :moveit_trajectory_interface/Follow "3d18b803936c9bc6d13b57b51b92d129")


