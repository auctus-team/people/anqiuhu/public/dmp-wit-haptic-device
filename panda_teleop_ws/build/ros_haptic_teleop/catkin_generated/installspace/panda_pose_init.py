#!/usr/bin/env python3.8
"""At startup, the panda arm is moved to the home position."""
import rospy
import time
from panda_motion import PandaMotion


rospy.wait_for_service('/panda/robot_state_publisher/get_loggers')

time.sleep(5)
try:
    panda = PandaMotion("/panda/robot_description", "panda", "panda_arm",load_gripper=True)
    X_0 = rospy.get_param("/panda/home/pose")
    quat_0 = rospy.get_param("/panda/home/orientation")

    target_pose = panda.getCurrentPose()
    target_pose.position.x = X_0[0]
    target_pose.position.y = X_0[1]
    target_pose.position.z = X_0[2]
    target_pose.orientation.w = quat_0[0]
    target_pose.orientation.x = quat_0[1]
    target_pose.orientation.y = quat_0[2]
    target_pose.orientation.z = quat_0[3]
    panda.goToPose(target_pose)
except rospy.ServiceException as e:
    print("Service call failed: %s"%e)
