# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# compile CXX with /usr/bin/c++
CXX_FLAGS =   -std=c++11 -std=gnu++14

CXX_DEFINES = -DBOOST_MPL_LIMIT_LIST_SIZE=30 -DBOOST_MPL_LIMIT_VECTOR_SIZE=30 -DHPP_FCL_HAS_OCTOMAP -DHPP_FCL_HAVE_OCTOMAP -DOCTOMAP_MAJOR_VERSION=1 -DOCTOMAP_MINOR_VERSION=9 -DOCTOMAP_PATCH_VERSION=8 -DPINOCCHIO_WITH_HPP_FCL -DPINOCCHIO_WITH_URDFDOM -DQT_NO_KEYWORDS -DROSCONSOLE_BACKEND_LOG4CXX -DROS_BUILD_SHARED_LIBS=1 -DROS_PACKAGE_NAME=\"ros_haptic_teleop\"

CXX_INCLUDES = -I/home/anqiu/ros_ws/panda_teleop_ws/build/ros_haptic_teleop/ros_haptic_teleop_autogen/include -I/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/moveit_trajectory_interface/include -I/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/franka_gripper/include -I/home/anqiu/ros_ws/panda_teleop_ws/src/franka_ros/franka_gripper/include -I/home/anqiu/ros_ws/panda_teleop_ws/src/moveit_trajectory_interface/include -I/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp -I/usr/include/OGRE/Overlay -I/usr/include/OGRE -I/usr/include/python3.8 -I/home/anqiu/ros_ws/panda_teleop_ws/src/ros_haptic_teleop/include -isystem /opt/ros/noetic/include -isystem /usr/include/eigen3 

