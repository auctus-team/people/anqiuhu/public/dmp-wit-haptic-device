# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "teleop_msgs: 0 messages, 6 services")

set(MSG_I_FLAGS "-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(teleop_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" "geometry_msgs/PoseArray:geometry_msgs/Point:geometry_msgs/Pose:std_msgs/Header:geometry_msgs/Quaternion"
)

get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" ""
)

get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" ""
)

get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" ""
)

get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" ""
)

get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_custom_target(_teleop_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "teleop_msgs" "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseArray.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_cpp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
)

### Generating Module File
_generate_module_cpp(teleop_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(teleop_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(teleop_msgs_generate_messages teleop_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_cpp _teleop_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(teleop_msgs_gencpp)
add_dependencies(teleop_msgs_gencpp teleop_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS teleop_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseArray.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)
_generate_srv_eus(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
)

### Generating Module File
_generate_module_eus(teleop_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(teleop_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(teleop_msgs_generate_messages teleop_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_eus _teleop_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(teleop_msgs_geneus)
add_dependencies(teleop_msgs_geneus teleop_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS teleop_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseArray.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)
_generate_srv_lisp(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
)

### Generating Module File
_generate_module_lisp(teleop_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(teleop_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(teleop_msgs_generate_messages teleop_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_lisp _teleop_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(teleop_msgs_genlisp)
add_dependencies(teleop_msgs_genlisp teleop_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS teleop_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseArray.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)
_generate_srv_nodejs(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
)

### Generating Module File
_generate_module_nodejs(teleop_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(teleop_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(teleop_msgs_generate_messages teleop_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_nodejs _teleop_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(teleop_msgs_gennodejs)
add_dependencies(teleop_msgs_gennodejs teleop_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS teleop_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseArray.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)
_generate_srv_py(teleop_msgs
  "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
)

### Generating Module File
_generate_module_py(teleop_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(teleop_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(teleop_msgs_generate_messages teleop_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/InterestPose.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/SDGains.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PFParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/GTParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/RGParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs/srv/PathParams.srv" NAME_WE)
add_dependencies(teleop_msgs_generate_messages_py _teleop_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(teleop_msgs_genpy)
add_dependencies(teleop_msgs_genpy teleop_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS teleop_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/teleop_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(teleop_msgs_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/teleop_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(teleop_msgs_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/teleop_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(teleop_msgs_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/teleop_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(teleop_msgs_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/teleop_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(teleop_msgs_generate_messages_py geometry_msgs_generate_messages_py)
endif()
