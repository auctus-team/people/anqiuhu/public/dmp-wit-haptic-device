#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/teleop_msgs:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/teleop_msgs/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/teleop_msgs/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/anqiu/ros_ws/panda_teleop_ws/build/teleop_msgs'
export PYTHONPATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/teleop_msgs/lib/python3/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/teleop_msgs/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/anqiu/ros_ws/panda_teleop_ws/src/teleop_msgs:$ROS_PACKAGE_PATH"