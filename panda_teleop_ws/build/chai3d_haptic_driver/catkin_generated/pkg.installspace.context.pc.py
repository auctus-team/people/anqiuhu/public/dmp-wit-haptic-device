# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;roscpp;rospy;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lchai3d_haptic_driver".split(';') if "-lchai3d_haptic_driver" != "" else []
PROJECT_NAME = "chai3d_haptic_driver"
PROJECT_SPACE_DIR = "/home/anqiu/ros_ws/panda_teleop_ws/install"
PROJECT_VERSION = "0.0.0"
