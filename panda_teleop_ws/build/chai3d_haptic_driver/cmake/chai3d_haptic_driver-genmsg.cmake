# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(WARNING "Invoking generate_messages() without having added any message or service file before.
You should either add add_message_files() and/or add_service_files() calls or remove the invocation of generate_messages().")
message(STATUS "chai3d_haptic_driver: 0 messages, 0 services")

set(MSG_I_FLAGS "")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(chai3d_haptic_driver_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_cpp(chai3d_haptic_driver
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/chai3d_haptic_driver
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(chai3d_haptic_driver_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(chai3d_haptic_driver_generate_messages chai3d_haptic_driver_generate_messages_cpp)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(chai3d_haptic_driver_gencpp)
add_dependencies(chai3d_haptic_driver_gencpp chai3d_haptic_driver_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS chai3d_haptic_driver_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_eus(chai3d_haptic_driver
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/chai3d_haptic_driver
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(chai3d_haptic_driver_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(chai3d_haptic_driver_generate_messages chai3d_haptic_driver_generate_messages_eus)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(chai3d_haptic_driver_geneus)
add_dependencies(chai3d_haptic_driver_geneus chai3d_haptic_driver_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS chai3d_haptic_driver_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_lisp(chai3d_haptic_driver
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/chai3d_haptic_driver
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(chai3d_haptic_driver_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(chai3d_haptic_driver_generate_messages chai3d_haptic_driver_generate_messages_lisp)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(chai3d_haptic_driver_genlisp)
add_dependencies(chai3d_haptic_driver_genlisp chai3d_haptic_driver_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS chai3d_haptic_driver_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_nodejs(chai3d_haptic_driver
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/chai3d_haptic_driver
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(chai3d_haptic_driver_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(chai3d_haptic_driver_generate_messages chai3d_haptic_driver_generate_messages_nodejs)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(chai3d_haptic_driver_gennodejs)
add_dependencies(chai3d_haptic_driver_gennodejs chai3d_haptic_driver_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS chai3d_haptic_driver_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services

### Generating Module File
_generate_module_py(chai3d_haptic_driver
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/chai3d_haptic_driver
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(chai3d_haptic_driver_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(chai3d_haptic_driver_generate_messages chai3d_haptic_driver_generate_messages_py)

# add dependencies to all check dependencies targets

# target for backward compatibility
add_custom_target(chai3d_haptic_driver_genpy)
add_dependencies(chai3d_haptic_driver_genpy chai3d_haptic_driver_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS chai3d_haptic_driver_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/chai3d_haptic_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/chai3d_haptic_driver
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/chai3d_haptic_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/chai3d_haptic_driver
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/chai3d_haptic_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/chai3d_haptic_driver
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/chai3d_haptic_driver)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/chai3d_haptic_driver
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/chai3d_haptic_driver)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/chai3d_haptic_driver\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/chai3d_haptic_driver
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
