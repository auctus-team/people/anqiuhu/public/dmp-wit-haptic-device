# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "actionlib;control_msgs;moveit_core;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lmoveit_simple_controller_manager_extended".split(';') if "-lmoveit_simple_controller_manager_extended" != "" else []
PROJECT_NAME = "moveit_simple_controller_manager_extended"
PROJECT_SPACE_DIR = "/home/anqiu/ros_ws/panda_teleop_ws/install"
PROJECT_VERSION = "1.1.6"
