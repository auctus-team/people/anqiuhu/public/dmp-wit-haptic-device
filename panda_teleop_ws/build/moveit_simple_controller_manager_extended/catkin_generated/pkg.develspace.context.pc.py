# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/anqiu/ros_ws/panda_teleop_ws/src/moveit_simple_controller_manager_extended/include".split(';') if "/home/anqiu/ros_ws/panda_teleop_ws/src/moveit_simple_controller_manager_extended/include" != "" else []
PROJECT_CATKIN_DEPENDS = "actionlib;control_msgs;moveit_core;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lmoveit_simple_controller_manager_extended".split(';') if "-lmoveit_simple_controller_manager_extended" != "" else []
PROJECT_NAME = "moveit_simple_controller_manager_extended"
PROJECT_SPACE_DIR = "/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/moveit_simple_controller_manager_extended"
PROJECT_VERSION = "1.1.6"
