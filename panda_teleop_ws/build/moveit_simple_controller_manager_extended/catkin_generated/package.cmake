set(_CATKIN_CURRENT_PACKAGE "moveit_simple_controller_manager_extended")
set(moveit_simple_controller_manager_extended_VERSION "1.1.6")
set(moveit_simple_controller_manager_extended_MAINTAINER "Michael Ferguson <mferguson@fetchrobotics.com>, Michael Görner <me@v4hn.de>, MoveIt Release Team <moveit_releasers@googlegroups.com>")
set(moveit_simple_controller_manager_extended_PACKAGE_FORMAT "2")
set(moveit_simple_controller_manager_extended_BUILD_DEPENDS "moveit_core" "roscpp" "pluginlib" "control_msgs" "actionlib")
set(moveit_simple_controller_manager_extended_BUILD_DEPENDS_pluginlib_VERSION_GTE "1.11.2")
set(moveit_simple_controller_manager_extended_BUILD_EXPORT_DEPENDS "moveit_core" "roscpp" "pluginlib" "control_msgs" "actionlib")
set(moveit_simple_controller_manager_extended_BUILD_EXPORT_DEPENDS_pluginlib_VERSION_GTE "1.11.2")
set(moveit_simple_controller_manager_extended_BUILDTOOL_DEPENDS "catkin")
set(moveit_simple_controller_manager_extended_BUILDTOOL_EXPORT_DEPENDS )
set(moveit_simple_controller_manager_extended_EXEC_DEPENDS "moveit_core" "roscpp" "pluginlib" "control_msgs" "actionlib")
set(moveit_simple_controller_manager_extended_EXEC_DEPENDS_pluginlib_VERSION_GTE "1.11.2")
set(moveit_simple_controller_manager_extended_RUN_DEPENDS "moveit_core" "roscpp" "pluginlib" "control_msgs" "actionlib")
set(moveit_simple_controller_manager_extended_RUN_DEPENDS_pluginlib_VERSION_GTE "1.11.2")
set(moveit_simple_controller_manager_extended_TEST_DEPENDS )
set(moveit_simple_controller_manager_extended_DOC_DEPENDS )
set(moveit_simple_controller_manager_extended_URL_WEBSITE "http://moveit.ros.org")
set(moveit_simple_controller_manager_extended_URL_BUGTRACKER "https://github.com/ros-planning/moveit/issues")
set(moveit_simple_controller_manager_extended_URL_REPOSITORY "https://github.com/ros-planning/moveit")
set(moveit_simple_controller_manager_extended_DEPRECATED "")