#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/setup_description:$CMAKE_PREFIX_PATH"
export PWD='/home/anqiu/ros_ws/panda_teleop_ws/build/setup_description'
export ROSLISP_PACKAGE_DIRECTORIES="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/setup_description/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/anqiu/ros_ws/panda_teleop_ws/src/setup_description:$ROS_PACKAGE_PATH"