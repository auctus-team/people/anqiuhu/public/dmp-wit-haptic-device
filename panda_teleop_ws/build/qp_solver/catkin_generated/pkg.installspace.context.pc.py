# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;qpOASES".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lqp_solver".split(';') if "-lqp_solver" != "" else []
PROJECT_NAME = "qp_solver"
PROJECT_SPACE_DIR = "/home/anqiu/ros_ws/panda_teleop_ws/install"
PROJECT_VERSION = "0.0.0"
