/****************************************************************************
** Meta object code from reading C++ file 'rviz_plug_ac.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "active_constraint/rviz_plug_ac.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rviz_plug_ac.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin_t {
    QByteArrayData data[39];
    char stringdata0[429];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin_t qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin = {
    {
QT_MOC_LITERAL(0, 0, 42), // "rviz_ac_plugins::Active_Const..."
QT_MOC_LITERAL(1, 43, 15), // "selectACservice"
QT_MOC_LITERAL(2, 59, 0), // ""
QT_MOC_LITERAL(3, 60, 11), // "std::string"
QT_MOC_LITERAL(4, 72, 7), // "ac_type"
QT_MOC_LITERAL(5, 80, 6), // "use_rg"
QT_MOC_LITERAL(6, 87, 15), // "Eigen::Vector3d"
QT_MOC_LITERAL(7, 103, 4), // "K_ac"
QT_MOC_LITERAL(8, 108, 4), // "B_ac"
QT_MOC_LITERAL(9, 113, 8), // "K_ac_obs"
QT_MOC_LITERAL(10, 122, 8), // "B_ac_obs"
QT_MOC_LITERAL(11, 131, 4), // "d_ac"
QT_MOC_LITERAL(12, 136, 8), // "d_ac_obs"
QT_MOC_LITERAL(13, 145, 1), // "a"
QT_MOC_LITERAL(14, 147, 1), // "b"
QT_MOC_LITERAL(15, 149, 1), // "c"
QT_MOC_LITERAL(16, 151, 11), // "sendSDGains"
QT_MOC_LITERAL(17, 163, 12), // "sendPFParams"
QT_MOC_LITERAL(18, 176, 12), // "sendGTParams"
QT_MOC_LITERAL(19, 189, 12), // "sendRGParams"
QT_MOC_LITERAL(20, 202, 14), // "sendPathParams"
QT_MOC_LITERAL(21, 217, 9), // "path_step"
QT_MOC_LITERAL(22, 227, 14), // "load_ac_params"
QT_MOC_LITERAL(23, 242, 13), // "add_ac_slider"
QT_MOC_LITERAL(24, 256, 8), // "ac_param"
QT_MOC_LITERAL(25, 265, 8), // "real_min"
QT_MOC_LITERAL(26, 274, 8), // "real_max"
QT_MOC_LITERAL(27, 283, 13), // "current_value"
QT_MOC_LITERAL(28, 297, 12), // "QVBoxLayout*"
QT_MOC_LITERAL(29, 310, 6), // "layout"
QT_MOC_LITERAL(30, 317, 10), // "slider_min"
QT_MOC_LITERAL(31, 328, 10), // "slider_max"
QT_MOC_LITERAL(32, 339, 15), // "slider_interval"
QT_MOC_LITERAL(33, 355, 12), // "QHBoxLayout*"
QT_MOC_LITERAL(34, 368, 12), // "configlayout"
QT_MOC_LITERAL(35, 381, 15), // "advanced_params"
QT_MOC_LITERAL(36, 397, 11), // "clearlayout"
QT_MOC_LITERAL(37, 409, 8), // "QLayout*"
QT_MOC_LITERAL(38, 418, 10) // "startIndex"

    },
    "rviz_ac_plugins::Active_Constraints_Plugin\0"
    "selectACservice\0\0std::string\0ac_type\0"
    "use_rg\0Eigen::Vector3d\0K_ac\0B_ac\0"
    "K_ac_obs\0B_ac_obs\0d_ac\0d_ac_obs\0a\0b\0"
    "c\0sendSDGains\0sendPFParams\0sendGTParams\0"
    "sendRGParams\0sendPathParams\0path_step\0"
    "load_ac_params\0add_ac_slider\0ac_param\0"
    "real_min\0real_max\0current_value\0"
    "QVBoxLayout*\0layout\0slider_min\0"
    "slider_max\0slider_interval\0QHBoxLayout*\0"
    "configlayout\0advanced_params\0clearlayout\0"
    "QLayout*\0startIndex"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_rviz_ac_plugins__Active_Constraints_Plugin[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,   11,  129,    2, 0x09 /* Protected */,
       1,   10,  152,    2, 0x29 /* Protected | MethodCloned */,
       1,    9,  173,    2, 0x29 /* Protected | MethodCloned */,
       1,    8,  192,    2, 0x29 /* Protected | MethodCloned */,
       1,    7,  209,    2, 0x29 /* Protected | MethodCloned */,
       1,    6,  224,    2, 0x29 /* Protected | MethodCloned */,
      16,    2,  237,    2, 0x09 /* Protected */,
      17,    5,  242,    2, 0x09 /* Protected */,
      18,    3,  253,    2, 0x09 /* Protected */,
      19,   10,  260,    2, 0x09 /* Protected */,
      20,    1,  281,    2, 0x09 /* Protected */,
      22,    0,  284,    2, 0x09 /* Protected */,
      23,    8,  285,    2, 0x09 /* Protected */,
      23,    7,  302,    2, 0x29 /* Protected | MethodCloned */,
      23,    6,  317,    2, 0x29 /* Protected | MethodCloned */,
      23,    5,  330,    2, 0x29 /* Protected | MethodCloned */,
      23,    8,  341,    2, 0x09 /* Protected */,
      23,    7,  358,    2, 0x29 /* Protected | MethodCloned */,
      23,    6,  373,    2, 0x29 /* Protected | MethodCloned */,
      23,    5,  386,    2, 0x29 /* Protected | MethodCloned */,
      34,    2,  397,    2, 0x09 /* Protected */,
      36,    2,  402,    2, 0x09 /* Protected */,
      36,    1,  407,    2, 0x29 /* Protected | MethodCloned */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float,    4,    5,    7,    8,    9,   10,   11,   12,   13,   14,   15,
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float,    4,    5,    7,    8,    9,   10,   11,   12,   13,   14,
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float, QMetaType::Float, QMetaType::Float,    4,    5,    7,    8,    9,   10,   11,   12,   13,
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float, QMetaType::Float,    4,    5,    7,    8,    9,   10,   11,   12,
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float,    4,    5,    7,    8,    9,   10,   11,
    QMetaType::Bool, 0x80000000 | 3, QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6,    4,    5,    7,    8,    9,   10,
    QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6,    7,    8,
    QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float,    7,    8,    9,   10,   11,
    QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float,    7,    8,   11,
    QMetaType::Bool, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 6, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float, 0x80000000 | 3,    7,    8,    9,   10,   11,   12,   13,   14,   15,    4,
    QMetaType::Bool, QMetaType::UInt,   21,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 28, QMetaType::Int, QMetaType::Int, QMetaType::Int,   24,   25,   26,   27,   29,   30,   31,   32,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 28, QMetaType::Int, QMetaType::Int,   24,   25,   26,   27,   29,   30,   31,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 28, QMetaType::Int,   24,   25,   26,   27,   29,   30,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 28,   24,   25,   26,   27,   29,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 33, QMetaType::Int, QMetaType::Int, QMetaType::Int,   24,   25,   26,   27,   29,   30,   31,   32,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 33, QMetaType::Int, QMetaType::Int,   24,   25,   26,   27,   29,   30,   31,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 33, QMetaType::Int,   24,   25,   26,   27,   29,   30,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Double, QMetaType::Double, QMetaType::Double, 0x80000000 | 33,   24,   25,   26,   27,   29,
    QMetaType::Void, 0x80000000 | 28, QMetaType::Bool,   29,   35,
    QMetaType::Void, 0x80000000 | 37, QMetaType::Int,   29,   38,
    QMetaType::Void, 0x80000000 | 37,   29,

       0        // eod
};

void rviz_ac_plugins::Active_Constraints_Plugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Active_Constraints_Plugin *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])),(*reinterpret_cast< float(*)>(_a[8])),(*reinterpret_cast< float(*)>(_a[9])),(*reinterpret_cast< float(*)>(_a[10])),(*reinterpret_cast< float(*)>(_a[11])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 1: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])),(*reinterpret_cast< float(*)>(_a[8])),(*reinterpret_cast< float(*)>(_a[9])),(*reinterpret_cast< float(*)>(_a[10])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])),(*reinterpret_cast< float(*)>(_a[8])),(*reinterpret_cast< float(*)>(_a[9])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 3: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])),(*reinterpret_cast< float(*)>(_a[8])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->selectACservice((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[5])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[6])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->sendSDGains((*reinterpret_cast< Eigen::Vector3d(*)>(_a[1])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->sendPFParams((*reinterpret_cast< Eigen::Vector3d(*)>(_a[1])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->sendGTParams((*reinterpret_cast< Eigen::Vector3d(*)>(_a[1])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: { bool _r = _t->sendRGParams((*reinterpret_cast< Eigen::Vector3d(*)>(_a[1])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[2])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[3])),(*reinterpret_cast< Eigen::Vector3d(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5])),(*reinterpret_cast< float(*)>(_a[6])),(*reinterpret_cast< float(*)>(_a[7])),(*reinterpret_cast< float(*)>(_a[8])),(*reinterpret_cast< float(*)>(_a[9])),(*reinterpret_cast< std::string(*)>(_a[10])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->sendPathParams((*reinterpret_cast< uint(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->load_ac_params(); break;
        case 12: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QVBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7])),(*reinterpret_cast< int(*)>(_a[8]))); break;
        case 13: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QVBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7]))); break;
        case 14: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QVBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 15: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QVBoxLayout*(*)>(_a[5]))); break;
        case 16: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QHBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7])),(*reinterpret_cast< int(*)>(_a[8]))); break;
        case 17: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QHBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7]))); break;
        case 18: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QHBoxLayout*(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 19: _t->add_ac_slider((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< QHBoxLayout*(*)>(_a[5]))); break;
        case 20: _t->configlayout((*reinterpret_cast< QVBoxLayout*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 21: _t->clearlayout((*reinterpret_cast< QLayout*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->clearlayout((*reinterpret_cast< QLayout*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVBoxLayout* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVBoxLayout* >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVBoxLayout* >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVBoxLayout* >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QHBoxLayout* >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QHBoxLayout* >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QHBoxLayout* >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 4:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QHBoxLayout* >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVBoxLayout* >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLayout* >(); break;
            }
            break;
        case 22:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLayout* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject rviz_ac_plugins::Active_Constraints_Plugin::staticMetaObject = { {
    &Custom_Slider::staticMetaObject,
    qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin.data,
    qt_meta_data_rviz_ac_plugins__Active_Constraints_Plugin,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *rviz_ac_plugins::Active_Constraints_Plugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *rviz_ac_plugins::Active_Constraints_Plugin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_rviz_ac_plugins__Active_Constraints_Plugin.stringdata0))
        return static_cast<void*>(this);
    return Custom_Slider::qt_metacast(_clname);
}

int rviz_ac_plugins::Active_Constraints_Plugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Custom_Slider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
