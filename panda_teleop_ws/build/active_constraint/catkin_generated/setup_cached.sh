#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/active_constraint:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/active_constraint/lib:$LD_LIBRARY_PATH"
export PWD='/home/anqiu/ros_ws/panda_teleop_ws/build/active_constraint'
export PYTHONPATH="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/active_constraint/lib/python3/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/anqiu/ros_ws/panda_teleop_ws/devel/.private/active_constraint/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/anqiu/ros_ws/panda_teleop_ws/src/active_constraint:$ROS_PACKAGE_PATH"