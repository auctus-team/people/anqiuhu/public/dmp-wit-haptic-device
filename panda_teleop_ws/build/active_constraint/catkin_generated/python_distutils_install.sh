#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/anqiu/ros_ws/panda_teleop_ws/src/active_constraint"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/anqiu/ros_ws/panda_teleop_ws/install/lib/python3/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/anqiu/ros_ws/panda_teleop_ws/install/lib/python3/dist-packages:/home/anqiu/ros_ws/panda_teleop_ws/build/active_constraint/lib/python3/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/anqiu/ros_ws/panda_teleop_ws/build/active_constraint" \
    "/usr/bin/python3.8" \
    "/home/anqiu/ros_ws/panda_teleop_ws/src/active_constraint/setup.py" \
     \
    build --build-base "/home/anqiu/ros_ws/panda_teleop_ws/build/active_constraint" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/anqiu/ros_ws/panda_teleop_ws/install" --install-scripts="/home/anqiu/ros_ws/panda_teleop_ws/install/bin"
