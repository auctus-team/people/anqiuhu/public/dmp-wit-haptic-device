#!/usr/bin/env python3
import pinocchio 
import rospy
import rospkg

import numpy as np

from active_constraint.srv import InterestPose
from panda_motion import PandaMotion

from moveit_msgs.msg import PositionConstraint

from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped

from numpy.linalg import norm, solve

import gg_interest_pose
import sys

def compute_cartesian_path(start_pose,goal_pose):
    panda = PandaMotion("/panda/robot_description", "panda", "panda_arm",load_gripper=True)
    # panda.stop()
    JOINT_ID = panda.group.get_joints().index('panda_joint7')
    panda.goToPoseRelative(x = -0.1, blocking=False)
    start_pose.position.x -= 0.1
    panda.goToPose(start_pose)  # next step will be to assure a safe go to initial pose without collision
    start_pose.position.x += 0.1
    panda.goToPose(start_pose)

    rospack = rospkg.RosPack()
    path = rospack.get_path('franka_description')
    model = pinocchio.buildModelFromUrdf(path + "/robots/panda.urdf")    

    oMdes_start = pinocchio.XYZQUATToSE3(np.array([start_pose.position.x,start_pose.position.y,start_pose.position.z,
                                             start_pose.orientation.w ,start_pose.orientation.x,start_pose.orientation.y, start_pose.orientation.z]))

    oMdes = pinocchio.XYZQUATToSE3(np.array([goal_pose.position.x,goal_pose.position.y,goal_pose.position.z,
                                                goal_pose.orientation.w, goal_pose.orientation.x ,goal_pose.orientation.y,goal_pose.orientation.z]))

    oMdes = oMdes_start.inverse()*oMdes
    data = model.createData()
    q = np.array(panda.group.get_current_joint_values())
    print("q_init",q)
    eps    = 1e-3
    IT_MAX = 1000
    DT     = 1e-1
    damp   = 1e-12

    i=0
    while True:
        pinocchio.forwardKinematics(model,data,q)
        iMd = data.oMi[JOINT_ID].actInv(oMdes)
        print("oMi",pinocchio.se3ToXYZQUAT(data.oMi[JOINT_ID]))
        err = pinocchio.log(iMd).vector  # in joint frame
        if norm(err) < eps:
            success = True
            break
        if i >= IT_MAX:
            success = False
            break
        J = pinocchio.computeJointJacobian(model,data,q,JOINT_ID)  # in joint frame
        J = -np.dot(pinocchio.Jlog6(iMd.inverse()), J)
        v = - J.T.dot(solve(J.dot(J.T) + damp * np.eye(6), err))
        q = pinocchio.integrate(model,q,v*DT)
        # if not i % 10:
        #     print('%d: error = %s' % (i, err.T))
        i += 1
        print("q",q)
    if success:
        print("Convergence achieved!")
    else:
        print("\nWarning: the iterative algorithm has not reached convergence to the desired precision")
    
    plan = panda.planJointSpace(q)
    panda.displayTrajectory(plan)

rospy.wait_for_service('/active_constraint/goals/X_gr')
try:
    goals_srv = rospy.ServiceProxy('/active_constraint/goals/X_gr',InterestPose)
    msg=PoseArray()
    msg.header.frame_id="world"
    

    if len(sys.argv) == 2:
        if sys.argv[1] == "scenario1":
            msg.poses = [Pose() for i in range(2)]
            msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], gg_interest_pose.get_pose_rc(7,5))
            msg.poses[1] = gg_interest_pose.fill_msg_pose(msg.poses[1], gg_interest_pose.get_pose_rc(3,8))
        elif sys.argv[1] == "scenario2":
            msg.poses = [Pose() for i in range(2)]
            msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], gg_interest_pose.get_pose_rc(7,5))
            msg.poses[1] = gg_interest_pose.fill_msg_pose(msg.poses[1], gg_interest_pose.get_pose_rc(3,8))
        elif sys.argv[1] == "scenario3":
            msg.poses = [Pose() for i in range(2)]
            msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], gg_interest_pose.get_pose_rc(7,5))
            msg.poses[1] = gg_interest_pose.fill_msg_pose(msg.poses[1], gg_interest_pose.get_pose_rc(3,8))
    elif len(sys.argv) == 4:
        if sys.argv[1] == "stand_alone":
            msg.poses = [Pose() for i in range(1)]
            msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], gg_interest_pose.get_pose_rc(int(sys.argv[2]), int(sys.argv[3])))


    msg.poses=[Pose() for i in range(1)]
    # msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], 
                                        # gg_interest_pose.get_pose_rc(7,5))
 
    msg.poses[0] = gg_interest_pose.fill_msg_pose(msg.poses[0], 
                                        gg_interest_pose.get_pose_rc(3,8))
    # msg.poses[1].position.x -= 0.05
    print("Publishing goals")
    goals_srv(msg)


except rospy.ServiceException as e:
    print("Service call failed: %s"%e)

    