# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/anqiu/ros_ws/panda_teleop_ws/src/torque_qp/src/controller.cpp" "/home/anqiu/ros_ws/panda_teleop_ws/build/torque_qp/CMakeFiles/torque_qp.dir/src/controller.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_MPL_LIMIT_LIST_SIZE=30"
  "BOOST_MPL_LIMIT_VECTOR_SIZE=30"
  "HPP_FCL_HAS_OCTOMAP"
  "HPP_FCL_HAVE_OCTOMAP"
  "OCTOMAP_MAJOR_VERSION=1"
  "OCTOMAP_MINOR_VERSION=9"
  "OCTOMAP_PATCH_VERSION=8"
  "PINOCCHIO_WITH_HPP_FCL"
  "PINOCCHIO_WITH_URDFDOM"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"torque_qp\""
  "torque_qp_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/anqiu/ros_ws/panda_teleop_ws/src/torque_qp/include"
  "/home/anqiu/ros_ws/panda_teleop_ws/src/qpOASES/qpOASES_svn/include"
  "/home/anqiu/ros_ws/panda_teleop_ws/src/qp_solver/include"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
