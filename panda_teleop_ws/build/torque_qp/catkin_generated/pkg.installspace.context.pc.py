# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include;/opt/ros/noetic/include".split(';') if "${prefix}/include;/opt/ros/noetic/include" != "" else []
PROJECT_CATKIN_DEPENDS = "qp_solver".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ltorque_qp;/opt/ros/noetic/lib/libpinocchio.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so".split(';') if "-ltorque_qp;/opt/ros/noetic/lib/libpinocchio.so;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;/usr/lib/x86_64-linux-gnu/libboost_serialization.so;/usr/lib/x86_64-linux-gnu/libboost_system.so" != "" else []
PROJECT_NAME = "torque_qp"
PROJECT_SPACE_DIR = "/home/anqiu/ros_ws/panda_teleop_ws/install"
PROJECT_VERSION = "0.0.1"
